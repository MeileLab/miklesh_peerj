%% model vs. observations: same class, +/- elevaiton, grayscale
% clear

load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/NDM20_Rs70_a10.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
NAT(:,7) = NAT(:,7) + 0.204;
observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];

% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/baseline')
cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/baseline')


% observation class index
Juncus = vertcat((1:5)',(24:28)',(49:53)',(74:78)',(99:103)',(124:128)',(149:153)',(174:178)',(199:203)',(224:228)',(249:253)',(274:278)',(299:303)',(324:328)',(349:353)',(374:378)');
MarshMeadow = vertcat((6:10)',(29:33)',(54:58)',(79:83)',(104:108)',(129:133)',(154:158)',(179:183)',(204:208)',(229:233)',(254:258)',(279:283)',(304:308)',(329:333)',(354:358)',(379:383)');
MedSpart = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)');
ShortSpart = vertcat((14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)');
TallSpart = vertcat((19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');


% Spartina = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)',...
%     (14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)',...
%     (19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

dz = 0.01;

% model index
mm = 9; % marsh meadow: 9-sarcorconia, 1-BF, 2-BM
for i = [3 6 7 8 mm]
    clear vindex elmt
    vindex = find(vegetation_class_complete == i);
    if i == 3
        for j = 1 : length(Juncus)
            [elmt] = abs(centroids(vindex,3)-NAT(Juncus(j,1),7)) <= dz;
            jrindex{j} = vindex(elmt);
        end
    end
    if i == 6
        for j = 1 : length(MedSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(MedSpart(j,1),7)) <= dz;
            msindex{j} = vindex(elmt);
        end
    end
    if i == 7
        for j = 1 : length(ShortSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(ShortSpart(j,1),7)) <= dz;
            ssindex{j} = vindex(elmt);
        end
    end
    if i == 8
        for j = 1 : length(TallSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(TallSpart(j,1),7)) <= dz;
            tsindex{j} = vindex(elmt);
        end
    end
    if i == mm
        for j = 1 : length(MarshMeadow)
            [elmt] = abs(centroids(vindex,3)-NAT(MarshMeadow(j,1),7)) <= dz;
            mmindex{j} = vindex(elmt);
        end
    end
end

ind(Juncus,1) = 3;
ind(MarshMeadow,1) = mm;
ind(MedSpart,1) = 6;
ind(ShortSpart,1) = 7;
ind(TallSpart,1) = 8;

dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021]+366;
% dayrecord = [171;198;269;280;315;351;385;420;454]+366;

dayobs(1:23) = dayrecord(1);
dayobs(24:48) = dayrecord(2);
dayobs(49:73) = dayrecord(3);
dayobs(74:98) = dayrecord(4);
dayobs(99:123) = dayrecord(5);
dayobs(124:148) = dayrecord(6);
dayobs(149:173) = dayrecord(7);
dayobs(174:198) = dayrecord(8);
dayobs(199:223) = dayrecord(9);
dayobs(224:248) = dayrecord(10);
dayobs(249:273) = dayrecord(11);
dayobs(274:298) = dayrecord(12);
dayobs(299:323) = dayrecord(13);
dayobs(324:348) = dayrecord(14);
dayobs(349:373) = dayrecord(15);
dayobs(374:398) = dayrecord(16);
dayobs = dayobs(:);
a = 0;
b = 0;
c = 0;
d = 0;
e = 0;

for i = 1 : length(dayobs)
    filename = strcat('day',num2str(dayobs(i)),'.mat');
    load(filename,'S1')
    if ind(i) == 3
        a = a + 1;
        Smodel(i,1) = nanmean(S1(1,jrindex{a}(:,1)));
        Smodel(i,2) = std(S1(1,jrindex{a}(:,1)));
        Smodel(i,3) = nanmedian(S1(1,jrindex{a}(:,1)));
    end
    if ind(i) == 6
        b = b + 1;
        Smodel(i,1) = nanmean(S1(1,msindex{b}(:,1)));
        Smodel(i,2) = std(S1(1,msindex{b}(:,1)));
        Smodel(i,3) = nanmedian(S1(1,msindex{b}(:,1)));
    end
    if ind(i) == 7
        c = c + 1;
        Smodel(i,1) = nanmean(S1(1,ssindex{c}(:,1)));
        Smodel(i,2) = std(S1(1,ssindex{c}(:,1)));
        Smodel(i,3) = nanmedian(S1(1,ssindex{c}(:,1)));
    end
    if ind(i) == 8
        d = d + 1;
        Smodel(i,1) = nanmean(S1(1,tsindex{d}(:,1)));
        Smodel(i,2) = std(S1(1,tsindex{d}(:,1)));
        Smodel(i,3) = nanmedian(S1(1,tsindex{d}(:,1)));
%         Smodel(i,1) = nan;
%         Smodel(i,2) = nan;
    end
    if ind(i) == mm
        e = e + 1;
        Smodel(i,1) = nanmean(S1(1,mmindex{e}(:,1)));
        Smodel(i,2) = std(S1(1,mmindex{e}(:,1)));
        Smodel(i,3) = nanmedian(S1(1,mmindex{e}(:,1)));
%         Smodel(i,3) = nan;
%         Smodel(i,1) = nan;
%         Smodel(i,2) = nan;
     end
end

marksize = 8;
TT = 1; % 1 is mean, 3 is median
figure
plot(Smodel(TallSpart(:,1),TT),NAT(TallSpart(:,1),3),'ko','MarkerSize',marksize); hold on
plot(Smodel(MedSpart(:,1),TT),NAT(MedSpart(:,1),3),'k^','MarkerSize',marksize); hold on
plot(Smodel(ShortSpart(:,1),TT),NAT(ShortSpart(:,1),3),'k+','MarkerSize',marksize); hold on
plot(Smodel(Juncus(:,1),TT),NAT(Juncus(:,1),3),'kp','MarkerSize',marksize); hold on
plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'kd','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
indgamma = ~isnan(Smodel(:,1));
gamma = Smodel(indgamma,1)\NAT(indgamma,3);

% gamma = Smodel(:,1)\NAT(:,3);

% plot(0:100,(0:100)*gamma,'k--')
grid on
grid minor
axis square
axis equal

xlim([0 100])
ylim([0 100])
% xlim([15 80])
% ylim([15 80])
% xlim([0 60])
% ylim([0 60])
% xlim([15 50])
% ylim([15 50])
legend('TS','MS','SS','JR','SV','1:1','Location','SouthEast')
% legend('TS','MS','SS','JR','1:1','Location','SouthEast')
% legend('TS','MS','SS','1:1','least-squares','Location','SouthEast')

set(gca,...
    'FontWeight','bold',...
    'FontSize',16)
% title(['same class, +/- elevation dz = ' num2str(dz) ' m, gamma = ' num2str(gamma)])

% figure
% plot(Smodel(:,1),NAT(:,3),'ko','MarkerSize',marksize); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% indgamma = ~isnan(Smodel(:,1));
% gamma = Smodel(indgamma,1)\NAT(indgamma,3);

% gamma = Smodel(:,1)\NAT(:,3);

% plot(0:100,(0:100)*gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% legend('','1:1','least-squares','Location','SouthEast')
% 
% 
% set(gca,...
%     'FontWeight','bold',...
%     'FontSize',16)
% yCalc1 = Smodel(indgamma,1)*gamma;
% y = NAT(indgamma,3);
% Rsq1 = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)

% figure
% plot(Smodel(TallSpart(:,1),3),NAT(TallSpart(:,1),3),'ko','MarkerSize',marksize); hold on
% plot(Smodel(MedSpart(:,1),3),NAT(MedSpart(:,1),3),'k^','MarkerSize',marksize); hold on
% plot(Smodel(ShortSpart(:,1),3),NAT(ShortSpart(:,1),3),'k+','MarkerSize',marksize); hold on
% plot(Smodel(Juncus(:,1),3),NAT(Juncus(:,1),3),'kp','MarkerSize',marksize); hold on
% plot(Smodel(MarshMeadow(:,1),3),NAT(MarshMeadow(:,1),3),'kd','MarkerSize',marksize); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% indgamma = ~isnan(Smodel(:,1));
% gamma = Smodel(indgamma,1)\NAT(indgamma,3);
% 
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% % plot(0:100,(0:100)*gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% % xlim([0 60])
% % ylim([0 60])
% % xlim([0 50])
% % ylim([0 50])
% legend('TS','MS','SS','JR','SV','1:1','Location','SouthEast')
% % legend('TS','MS','SS','JR','1:1','least-squares','Location','SouthEast')
% % legend('TS','MS','SS','1:1','least-squares','Location','SouthEast')
% 
% set(gca,...
%     'FontWeight','bold',...
%     'FontSize',16)
% % title(['same class, +/- elevation dz = ' num2str(dz) ' m, gamma = ' num2str(gamma)])
%% model vs. observations: same class, +/- elevaiton, grayscale VALIDATION AREA
% clear
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/NDM20_Rs70_a10.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
NAT(:,7) = NAT(:,7) + 0.204;
observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/wrack_validation_area_elements.mat')

% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/baseline')
cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/baseline')


% observation class index
Juncus = vertcat((1:5)',(24:28)',(49:53)',(74:78)',(99:103)',(124:128)',(149:153)',(174:178)',(199:203)',(224:228)',(249:253)',(274:278)',(299:303)',(324:328)',(349:353)',(374:378)');
MarshMeadow = vertcat((6:10)',(29:33)',(54:58)',(79:83)',(104:108)',(129:133)',(154:158)',(179:183)',(204:208)',(229:233)',(254:258)',(279:283)',(304:308)',(329:333)',(354:358)',(379:383)');
MedSpart = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)');
ShortSpart = vertcat((14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)');
TallSpart = vertcat((19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

Spartina = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)',...
    (14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)',...
    (19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

points_in_domain = validation_area_elements;
dz = 0.01;

% model index
mm = 9; % marsh meadow: 9-sarcorconia, 1-BF, 2-BM
for i = [3 6 7 8 mm]
    clear vindex elmt
    vindex = find(vegetation_class_complete == i);
    if i == 3
        for j = 1 : length(Juncus)
            [elmt] = abs(centroids(vindex,3)-NAT(Juncus(j,1),7)) <= dz;
            jrindex{j} = vindex(elmt);
        end
    end
    if i == 6
        for j = 1 : length(MedSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(MedSpart(j,1),7)) <= dz;
            msindex{j} = vindex(elmt);
        end
    end
    if i == 7
        for j = 1 : length(ShortSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(ShortSpart(j,1),7)) <= dz;
            ssindex{j} = vindex(elmt);
        end
    end
    if i == 8
        for j = 1 : length(TallSpart)
            [elmt] = abs(centroids(vindex,3)-NAT(TallSpart(j,1),7)) <= dz;
            tsindex{j} = vindex(elmt);
        end
    end
    if i == mm
        for j = 1 : length(MarshMeadow)
            [elmt] = abs(centroids(vindex,3)-NAT(MarshMeadow(j,1),7)) <= dz;
            mmindex{j} = vindex(elmt);
        end
    end
end

ind(Juncus,1) = 3;
ind(MarshMeadow,1) = mm;
ind(MedSpart,1) = 6;
ind(ShortSpart,1) = 7;
ind(TallSpart,1) = 8;

dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021]+366;

dayobs(1:23) = dayrecord(1);
dayobs(24:48) = dayrecord(2);
dayobs(49:73) = dayrecord(3);
dayobs(74:98) = dayrecord(4);
dayobs(99:123) = dayrecord(5);
dayobs(124:148) = dayrecord(6);
dayobs(149:173) = dayrecord(7);
dayobs(174:198) = dayrecord(8);
dayobs(199:223) = dayrecord(9);
dayobs(224:248) = dayrecord(10);
dayobs(249:273) = dayrecord(11);
dayobs(274:298) = dayrecord(12);
dayobs(299:323) = dayrecord(13);
dayobs(324:348) = dayrecord(14);
dayobs(349:373) = dayrecord(15);
dayobs(374:398) = dayrecord(16);
dayobs = dayobs(:);
a = 0;
b = 0;
c = 0;
d = 0;
e = 0;

for i = 1 : length(dayobs)
    filename = strcat('day',num2str(dayobs(i)),'.mat');
    load(filename,'S1')
    if ind(i) == 3
        a = a + 1;
        A = intersect(points_in_domain,jrindex{a}(:,1));
        Smodel(i,1) = nanmean(S1(1,A));
        Smodel(i,2) = std(S1(1,A));
        Smodel(i,3) = nanmedian(S1(1,A));
        Smodel(i,4) = length(A);
    end
    if ind(i) == 6
        b = b + 1;
        B = intersect(points_in_domain,msindex{b}(:,1));
        Smodel(i,1) = nanmean(S1(1,B));
        Smodel(i,2) = std(S1(1,B));
        Smodel(i,3) = nanmedian(S1(1,B));
        Smodel(i,4) = length(B);
    end
    if ind(i) == 7
        c = c + 1;
        C = intersect(points_in_domain,ssindex{c}(:,1));
        Smodel(i,1) = nanmean(S1(1,C));
        Smodel(i,2) = std(S1(1,C));
        Smodel(i,3) = nanmedian(S1(1,C));
        Smodel(i,4) = length(C);
    end
    if ind(i) == 8
        d = d + 1;
        D = intersect(points_in_domain,tsindex{d}(:,1));
        Smodel(i,1) = nanmean(S1(1,D));
        Smodel(i,2) = std(S1(1,D));
        Smodel(i,3) = nanmedian(S1(1,D));
        Smodel(i,4) = length(D);
%         Smodel(i,1) = nan;
%         Smodel(i,2) = nan;
    end
    if ind(i) == mm
        e = e + 1;
%         E = intersect(points_in_domain,mmindex{e}(:,1));
%         Smodel(i,1) = nanmean(S1(1,E));
%         Smodel(i,2) = std(S1(1,E));
%         Smodel(i,3) = nanmedian(S1(1,E));
%         Smodel(i,4) = length(E);
        Smodel(i,3) = nan;
        Smodel(i,1) = nan;
        Smodel(i,2) = nan;
     end
end

marksize = 8;
TT = 1; % 1 is mean, 3 is median
figure
plot(Smodel(TallSpart(:,1),TT),NAT(TallSpart(:,1),3),'ko','MarkerSize',marksize); hold on
plot(Smodel(MedSpart(:,1),TT),NAT(MedSpart(:,1),3),'k^','MarkerSize',marksize); hold on
plot(Smodel(ShortSpart(:,1),TT),NAT(ShortSpart(:,1),3),'k+','MarkerSize',marksize); hold on
plot(Smodel(Juncus(:,1),TT),NAT(Juncus(:,1),3),'kp','MarkerSize',marksize); hold on
% plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'kd','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
indgamma = ~isnan(Smodel(:,1));
gamma = Smodel(indgamma,1)\NAT(indgamma,3);

% gamma = Smodel(:,1)\NAT(:,3);

% plot(0:100,(0:100)*gamma,'k--')
grid on
grid minor
axis square
axis equal

% xlim([0 100])
% ylim([0 100])
% xlim([15 80])
% ylim([15 80])
% xlim([0 60])
% ylim([0 60])
xlim([15 50])
ylim([15 50])
% legend('TS','MS','SS','JR','SV','1:1','Location','SouthEast')
legend('TS','MS','SS','JR','1:1','Location','SouthEast')
% legend('TS','MS','SS','1:1','least-squares','Location','SouthEast')

set(gca,...
    'FontWeight','bold',...
    'FontSize',16) 
%% MEDIAN or MEAN
xx = 1; % 1 = mean; 3 = median
yCalc1 = Smodel(TallSpart(:,1),xx);
y = NAT(TallSpart(:,1),3);
% RsqTS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
mdlTS = fitlm(yCalc1,y,'Intercept',false)

yCalc1 = Smodel(MedSpart(:,1),xx);
y = NAT(MedSpart(:,1),3);
% RsqMS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
mdlMS = fitlm(yCalc1,y,'Intercept',false)

yCalc1 = Smodel(ShortSpart(:,1),xx);
y = NAT(ShortSpart(:,1),3);
% RsqSS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
mdlSS = fitlm(yCalc1,y,'Intercept',false)

yCalc1 = Smodel(Juncus(:,1),xx);
y = NAT(Juncus(:,1),3);
% RsqJR = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
mdlJR = fitlm(yCalc1,y,'Intercept',false)

% yCalc1 = Smodel(MarshMeadow(:,1),xx);
% y = NAT(MarshMeadow(:,1),3);
% % RsqMM = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlMM = fitlm(yCalc1,y,'Intercept',false)

yCalc1 = Smodel(Spartina(:,1),xx);
y = NAT(Spartina(:,1),3);
% RsqMM = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
mdlSpart = fitlm(yCalc1,y,'Intercept',false)

%% MEAN
% yCalc1 = Smodel(indgamma,1);
% y = NAT(indgamma,3);
% mdl = fitlm(yCalc1,y,'Intercept',false)
% 
% yCalc1 = Smodel(TallSpart(:,1),1);
% y = NAT(TallSpart(:,1),3);
% RsqTS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlTS = fitlm(yCalc1,y,'Intercept',false)
% 
% yCalc1 = Smodel(MedSpart(:,1),1);
% y = NAT(MedSpart(:,1),3);
% RsqMS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlMS = fitlm(yCalc1,y,'Intercept',false)
% 
% yCalc1 = Smodel(ShortSpart(:,1),1);
% y = NAT(ShortSpart(:,1),3);
% RsqSS = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlSS = fitlm(yCalc1,y,'Intercept',false)
% 
% yCalc1 = Smodel(Juncus(:,1),1);
% y = NAT(Juncus(:,1),3);
% RsqJR = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlJR = fitlm(yCalc1,y,'Intercept',false)
% 
% yCalc1 = Smodel(MarshMeadow(:,1),1);
% y = NAT(MarshMeadow(:,1),3);
% RsqMM = 1 - sum((y - yCalc1).^2)/sum((y - mean(y)).^2)
% mdlMM = fitlm(yCalc1,y,'Intercept',false)
%% model vs. observations
% clear
% 
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
% NAT(:,7) = NAT(:,7) + 0.204;
% observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];
% 
% % cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ndm20_diff5cm_drainage')
% % cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/field90/')
% % cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/MMEToneandquarter/')
% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/30cm/')
% 
% 
% 
% points_in_region = GCE10pointsNDM20;
% 
% % Load data
% maxElevation = 1.3;
% dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021];
% dayobs(1:23) = dayrecord(1);
% dayobs(24:48) = dayrecord(2);
% dayobs(49:73) = dayrecord(3);
% dayobs(74:98) = dayrecord(4);
% dayobs(99:123) = dayrecord(5);
% dayobs(124:148) = dayrecord(6);
% dayobs(149:173) = dayrecord(7);
% dayobs(174:198) = dayrecord(8);
% dayobs(199:223) = dayrecord(9);
% dayobs(224:248) = dayrecord(10);
% dayobs(249:273) = dayrecord(11);
% dayobs(274:298) = dayrecord(12);
% dayobs(299:323) = dayrecord(13);
% dayobs(324:348) = dayrecord(14);
% dayobs(349:373) = dayrecord(15);
% dayobs(374:398) = dayrecord(16);
% dayobs = dayobs(:);
% 
% 
% % find nearest element to observation locations
% % for i = 1 : length(NAT)
%     x_dist = centroids(:,1)-NAT(i,1);
%     y_dist = centroids(:,2)-NAT(i,2);
%     dist = sqrt(x_dist.^2+y_dist.^2);
%     [a,b] = min(dist);
%     
%     ind_dist(i,1) = b; % index
%     ind_dist(i,2) = a; % distance (m)
% end
% 
% for i = 1 : length(dayobs)
%     filename = strcat('day',num2str(dayobs(i)),'.mat');
%     load(filename,'S1')
%     Smodel(i,1) = S1(ind_dist(i,1));
% end
% 
% figure
% plot(Smodel(:,1),NAT(:,3),'.b'); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100])
% gamma = NAT(:,3)\Smodel(:,1);
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% plot(0:100,(0:100)/gamma,'.r')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% legend('','1:1','gamma-fit','Location','East')
% title('nearest element')
% 
% % zdiff = NAT(:,7) - centroids(ind_dist(:,1),3);
% % sdiff = NAT(:,3) - Smodel;
% %
% % figure
% % plot(zdiff,sdiff,'.')
% % xlabel('z difference (m)')
% % ylabel('salinity difference')
% % grid on
% % grid minor
%% model vs. observations: nearest element, same class
% clear
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/NDM20_Rs70_a10.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
NAT(:,7) = NAT(:,7) + 0.204;
observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];

% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/baseline')
cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/baseline')



points_in_region = GCE10pointsNDM20;

% Load data
dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021]+366;
dayobs(1:23) = dayrecord(1);
dayobs(24:48) = dayrecord(2);
dayobs(49:73) = dayrecord(3);
dayobs(74:98) = dayrecord(4);
dayobs(99:123) = dayrecord(5);
dayobs(124:148) = dayrecord(6);
dayobs(149:173) = dayrecord(7);
dayobs(174:198) = dayrecord(8);
dayobs(199:223) = dayrecord(9);
dayobs(224:248) = dayrecord(10);
dayobs(249:273) = dayrecord(11);
dayobs(274:298) = dayrecord(12);
dayobs(299:323) = dayrecord(13);
dayobs(324:348) = dayrecord(14);
dayobs(349:373) = dayrecord(15);
dayobs(374:398) = dayrecord(16);
dayobs = dayobs(:);

% observation class index
Juncus = vertcat((1:5)',(24:28)',(49:53)',(74:78)',(99:103)',(124:128)',(149:153)',(174:178)',(199:203)',(224:228)',(249:253)',(274:278)',(299:303)',(324:328)',(349:353)',(374:378)');
MarshMeadow = vertcat((6:10)',(29:33)',(54:58)',(79:83)',(104:108)',(129:133)',(154:158)',(179:183)',(204:208)',(229:233)',(254:258)',(279:283)',(304:308)',(329:333)',(354:358)',(379:383)');
MedSpart = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)');
ShortSpart = vertcat((14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)');
TallSpart = vertcat((19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

Spartina = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)',...
    (14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)',...
    (19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

% find nearest element to observation locations
for i = 1 : length(NAT)
    x_dist = centroids(:,1)-NAT(i,1);
    y_dist = centroids(:,2)-NAT(i,2);
    dist = sqrt(x_dist.^2+y_dist.^2);
    [a,b] = min(dist);
    
    ind_dist(i,1) = b; % index
    ind_dist(i,2) = a; % distance (m)
    ind_dist(i,3) = vegetation_class_complete(b);
end

ind_dist(Juncus,4) = 3;
ind_dist(MarshMeadow,4) = 9;
ind_dist(MedSpart,4) = 6;
ind_dist(ShortSpart,4) = 7;
ind_dist(TallSpart,4) = 8;


for i = 1 : length(dayobs)
    filename = strcat('day',num2str(dayobs(i)),'.mat');
    load(filename,'S1')
    if ind_dist(i,3) == ind_dist(i,4)
        Smodel(i,1) = S1(ind_dist(i,1));
    else
        Smodel(i,1) = nan;
    end
end
marksize = 8;
TT = 1; % 1 is mean, 3 is median
figure
plot(Smodel(TallSpart(:,1),TT),NAT(TallSpart(:,1),3),'ko','MarkerSize',marksize); hold on
plot(Smodel(MedSpart(:,1),TT),NAT(MedSpart(:,1),3),'k^','MarkerSize',marksize); hold on
plot(Smodel(ShortSpart(:,1),TT),NAT(ShortSpart(:,1),3),'k+','MarkerSize',marksize); hold on
plot(Smodel(Juncus(:,1),TT),NAT(Juncus(:,1),3),'kp','MarkerSize',marksize); hold on
% plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'kd','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
indgamma = ~isnan(Smodel(:,1));
gamma = Smodel(indgamma,1)\NAT(indgamma,3);

% gamma = Smodel(:,1)\NAT(:,3);

% plot(0:100,(0:100)*gamma,'k--')
grid on
grid minor
axis square
axis equal

% xlim([0 100])
% ylim([0 100])
% xlim([15 80])
% ylim([15 80])
% xlim([0 60])
% ylim([0 60])
xlim([15 50])
ylim([15 50])
% legend('TS','MS','SS','JR','SV','1:1','Location','SouthEast')
legend('TS','MS','SS','JR','1:1','Location','SouthEast')
title('nearest element, same class')
% legend('TS','MS','SS','1:1','least-squares','Location','SouthEast')

set(gca,...
    'FontWeight','bold',...
    'FontSize',16)

% figure
% plot(Smodel(TallSpart(:,1),1),NAT(TallSpart(:,1),3),'.b'); hold on
% plot(Smodel(MedSpart(:,1),1),NAT(MedSpart(:,1),3),'.r'); hold on
% plot(Smodel(ShortSpart(:,1),1),NAT(ShortSpart(:,1),3),'.g'); hold on
% plot(Smodel(Juncus(:,1),1),NAT(Juncus(:,1),3),'.c'); hold on
% plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'.m'); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% indgamma = ~isnan(Smodel);
% gamma = NAT(indgamma,3)\Smodel(indgamma,1);
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% plot(0:100,(0:100)/gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% legend('TS','MS','SS','JR','SV','1:1','least-squares','Location','East')
% title('nearest element, same class')
% 
% figure
% plot(Smodel(TallSpart(:,1),1),NAT(TallSpart(:,1),3),'.b'); hold on
% plot(Smodel(MedSpart(:,1),1),NAT(MedSpart(:,1),3),'.r'); hold on
% plot(Smodel(ShortSpart(:,1),1),NAT(ShortSpart(:,1),3),'.g'); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% blerg(Spartina,1) = 1;
% blerg = logical(blerg);
% I = (~isnan(Smodel)).* blerg;
% I = logical(I);
% gamma = NAT(I,3)\Smodel(I,1);
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% plot(0:100,(0:100)/gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 50])
% ylim([0 50])
% legend('TS','MS','SS','1:1','least-squares','Location','East')
% title('nearest element, same class')
%% model vs. observations: same class, nearest elevaiton
% clear

load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/NDM20_Rs70_a10.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
NAT(:,7) = NAT(:,7) + 0.204;
observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];

cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/baseline')
% cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/baseline')

% observation class index
Juncus = vertcat((1:5)',(24:28)',(49:53)',(74:78)',(99:103)',(124:128)',(149:153)',(174:178)',(199:203)',(224:228)',(249:253)',(274:278)',(299:303)',(324:328)',(349:353)',(374:378)');
MarshMeadow = vertcat((6:10)',(29:33)',(54:58)',(79:83)',(104:108)',(129:133)',(154:158)',(179:183)',(204:208)',(229:233)',(254:258)',(279:283)',(304:308)',(329:333)',(354:358)',(379:383)');
MedSpart = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)');
ShortSpart = vertcat((14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)');
TallSpart = vertcat((19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');

Spartina = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)',...
    (14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)',...
    (19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');


% model index
for i = [3 6 7 8 9]
    clear vindex z_delta elmt
    vindex = find(vegetation_class_complete == i);
    if i == 3
        for j = 1 : length(Juncus)
            [z_delta(j,1),elmt(j,1)] = min(abs(centroids(vindex,3)-NAT(Juncus(j,1),7)));
        end
        jrindex = vindex(elmt(:,1));
    end
    if i == 6
        for j = 1 : length(MedSpart)
            [z_delta(j,1),elmt(j,1)] = min(abs(centroids(vindex,3)-NAT(MedSpart(j,1),7)));
        end
        msindex = vindex(elmt(:,1));
    end
    if i == 7
        for j = 1 : length(ShortSpart)
            [z_delta(j,1),elmt(j,1)] = min(abs(centroids(vindex,3)-NAT(ShortSpart(j,1),7)));
        end
        ssindex = vindex(elmt(:,1));
    end
    if i == 8
        for j = 1 : length(TallSpart)
            [z_delta(j,1),elmt(j,1)] = min(abs(centroids(vindex,3)-NAT(TallSpart(j,1),7)));
        end
        tsindex = vindex(elmt(:,1));
    end
    if i == 9
        for j = 1 : length(MarshMeadow)
            [z_delta(j,1),elmt(j,1)] = min(abs(centroids(vindex,3)-NAT(MarshMeadow(j,1),7)));
        end
        mmindex = vindex(elmt(:,1));
    end
end

ind(Juncus,1) = 3;
ind(MarshMeadow,1) = 9;
ind(MedSpart,1) = 6;
ind(ShortSpart,1) = 7;
ind(TallSpart,1) = 8;

% Load data
dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021]+366;

dayobs(1:23) = dayrecord(1);
dayobs(24:48) = dayrecord(2);
dayobs(49:73) = dayrecord(3);
dayobs(74:98) = dayrecord(4);
dayobs(99:123) = dayrecord(5);
dayobs(124:148) = dayrecord(6);
dayobs(149:173) = dayrecord(7);
dayobs(174:198) = dayrecord(8);
dayobs(199:223) = dayrecord(9);
dayobs(224:248) = dayrecord(10);
dayobs(249:273) = dayrecord(11);
dayobs(274:298) = dayrecord(12);
dayobs(299:323) = dayrecord(13);
dayobs(324:348) = dayrecord(14);
dayobs(349:373) = dayrecord(15);
dayobs(374:398) = dayrecord(16);
dayobs = dayobs(:);

a = 0;
b = 0;
c = 0;
d = 0;
e = 0;
for i = 1 : length(dayobs)
    filename = strcat('day',num2str(dayobs(i)),'.mat');
    load(filename,'S1')
    if ind(i) == 3
        a = a + 1;
        Smodel(i,1) = S1(1,jrindex(a));
    end
    if ind(i) == 6
        b = b + 1;
        Smodel(i,1) = S1(1,msindex(b));
    end
    if ind(i) == 7
        c = c + 1;
        Smodel(i,1) = S1(1,ssindex(c));
    end
    if ind(i) == 8
        d = d + 1;
        Smodel(i,1) = S1(1,tsindex(d));
    end
    if ind(i) == 9
        e = e + 1;
        Smodel(i,1) = S1(1,mmindex(e));
    end
end
marksize = 8;
TT = 1; % 1 is mean, 3 is median
figure
plot(Smodel(TallSpart(:,1),TT),NAT(TallSpart(:,1),3),'ko','MarkerSize',marksize); hold on
plot(Smodel(MedSpart(:,1),TT),NAT(MedSpart(:,1),3),'k^','MarkerSize',marksize); hold on
plot(Smodel(ShortSpart(:,1),TT),NAT(ShortSpart(:,1),3),'k+','MarkerSize',marksize); hold on
plot(Smodel(Juncus(:,1),TT),NAT(Juncus(:,1),3),'kp','MarkerSize',marksize); hold on
% plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'kd','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
title('nsame class, closest elevation')

line([0 100],[0 100],'Color','k','LineStyle','-')
indgamma = ~isnan(Smodel(:,1));
gamma = Smodel(indgamma,1)\NAT(indgamma,3);

% gamma = Smodel(:,1)\NAT(:,3);

% plot(0:100,(0:100)*gamma,'k--')
grid on
grid minor
axis square
axis equal

% xlim([0 100])
% ylim([0 100])
% xlim([15 80])
% ylim([15 80])
% xlim([0 60])
% ylim([0 60])
xlim([15 50])
ylim([15 50])
% legend('TS','MS','SS','JR','SV','1:1','Location','SouthEast')
legend('TS','MS','SS','JR','1:1','Location','SouthEast')
% legend('TS','MS','SS','1:1','least-squares','Location','SouthEast')

set(gca,...
    'FontWeight','bold',...
    'FontSize',16)

% figure
% plot(Smodel(TallSpart(:,1),1),NAT(TallSpart(:,1),3),'.b'); hold on
% plot(Smodel(MedSpart(:,1),1),NAT(MedSpart(:,1),3),'.r'); hold on
% plot(Smodel(ShortSpart(:,1),1),NAT(ShortSpart(:,1),3),'.g'); hold on
% plot(Smodel(Juncus(:,1),1),NAT(Juncus(:,1),3),'.c'); hold on
% plot(Smodel(MarshMeadow(:,1),1),NAT(MarshMeadow(:,1),3),'.m'); hold on
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% indgamma = ~isnan(Smodel);
% gamma = NAT(indgamma,3)\Smodel(indgamma,1);
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% plot(0:100,(0:100)/gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% legend('TS','MS','SS','JR','SV','1:1','least-squares','Location','East')
% title(['same class, closest elevation, gamma = ' num2str(gamma)])

%% CHECK VARIATION ON COMPUTED MEANS
% [mean],[std],[median],[obs],[year],[elevation],[CV]
outTS = [Smodel(TallSpart(:,1),1:3),NAT(TallSpart(:,1),3),NAT(TallSpart(:,1),6:7)];
outMS = [Smodel(MedSpart(:,1),1:3),NAT(MedSpart(:,1),3),NAT(MedSpart(:,1),6:7)];
outSS = [Smodel(ShortSpart(:,1),1:3),NAT(ShortSpart(:,1),3),NAT(ShortSpart(:,1),6:7)];
outJR = [Smodel(Juncus(:,1),1:3),NAT(Juncus(:,1),3),NAT(Juncus(:,1),6:7)];
outMM = [Smodel(MarshMeadow(:,1),1:3),NAT(MarshMeadow(:,1),3),NAT(MarshMeadow(:,1),6:7)];

outTS(:,7) = outTS(:,2)./outTS(:,1);
outMS(:,7) = outMS(:,2)./outMS(:,1);
outSS(:,7) = outSS(:,2)./outSS(:,1);
outJR(:,7) = outJR(:,2)./outJR(:,1);
outMM(:,7) = outMM(:,2)./outMM(:,1);

figure
subplot(3,2,1)
plot(outTS(:,4),outTS(:,7),'.'); title('TS'); xlabel('obs.'); ylabel('CV')
axis square
subplot(3,2,2)
plot(outMS(:,4),outMS(:,7),'.'); title('MS'); xlabel('obs.'); ylabel('CV')
axis square
subplot(3,2,3)
plot(outSS(:,4),outSS(:,7),'.'); title('SS'); xlabel('obs.'); ylabel('CV')
axis square
subplot(3,2,4)
plot(outJR(:,4),outJR(:,7),'.'); title('JR'); xlabel('obs.'); ylabel('CV')
axis square
subplot(3,2,5)
plot(outMM(:,4),outMM(:,7),'.'); title('MM'); xlabel('obs.'); ylabel('CV')
axis square


figure
subplot(3,2,1)
plot(outTS(:,4),outTS(:,2),'.'); title('TS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,2)
plot(outMS(:,4),outMS(:,2),'.'); title('MS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,3)
plot(outSS(:,4),outSS(:,2),'.'); title('SS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,4)
plot(outJR(:,4),outJR(:,2),'.'); title('JR'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,5)
plot(outMM(:,4),outMM(:,2),'.'); title('MM'); xlabel('obs.'); ylabel('STD')
axis square


figure
subplot(3,2,1)
plot(outTS(:,1),outTS(:,3),'.'); title('TS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,2)
plot(outMS(:,1),outMS(:,3),'.'); title('MS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,3)
plot(outSS(:,1),outSS(:,3),'.'); title('SS'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,4)
plot(outJR(:,1),outJR(:,3),'.'); title('JR'); xlabel('obs.'); ylabel('STD')
axis square
subplot(3,2,5)
plot(outMM(:,1),outMM(:,3),'.'); title('MM'); xlabel('obs.'); ylabel('STD')
axis square

%% juncus
figure
plot(Smodel(1:5,1),NAT(1:5,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('6/18/2012')

figure
plot(Smodel(24:28,1),NAT(24:28,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/15/2012')

figure
plot(Smodel(49:53,1),NAT(49:53,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('8/24/2012')

figure
plot(Smodel(74:78,1),NAT(74:78,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/5/2012')

figure
plot(Smodel(99:103,1),NAT(99:103,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('11/9/2012')

figure
plot(Smodel(124:128,1),NAT(124:128,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('12/15/2012')

figure
plot(Smodel(149:153,1),NAT(149:153,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('1/18/2013')

figure
plot(Smodel(174:178,1),NAT(174:178,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('2/22/2013')

figure
plot(Smodel(199:203,1),NAT(199:203,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('3/28/2013')

figure
plot(Smodel(224:228,1),NAT(224:228,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('6/2/2013')

figure
plot(Smodel(249:253,1),NAT(249:253,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/15/2013')

figure
plot(Smodel(274:278,1),NAT(274:278,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/1/2013')

figure
plot(Smodel(299:303,1),NAT(299:303,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('3/13/2014')

figure
plot(Smodel(324:328,1),NAT(324:328,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('5/25/2014')

figure
plot(Smodel(349:353,1),NAT(349:353,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/14/2014')

figure
plot(Smodel(374:378,1),NAT(374:378,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/17/2014')
%% mm
figure
plot(Smodel(6:10,1),NAT(6:10,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('6/18/2012')

figure
plot(Smodel(29:33,1),NAT(29:33,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/15/2012')

figure
plot(Smodel(54:58,1),NAT(54:58,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('8/24/2012')

figure
plot(Smodel(79:83,1),NAT(79:83,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/5/2012')

figure
plot(Smodel(104:108,1),NAT(104:108,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('11/9/2012')

figure
plot(Smodel(129:133,1),NAT(129:133,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('12/15/2012')

figure
plot(Smodel(154:158,1),NAT(154:158,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('1/18/2013')

figure
plot(Smodel(179:183,1),NAT(179:183,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('2/22/2013')

figure
plot(Smodel(204:208,1),NAT(204:208,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('3/28/2013')

figure
plot(Smodel(229:233,1),NAT(229:233,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('6/2/2013')

figure
plot(Smodel(254:258,1),NAT(254:258,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/15/2013')

figure
plot(Smodel(279:283,1),NAT(279:283,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/1/2013')

figure
plot(Smodel(304:308,1),NAT(304:308,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('3/13/2014')

figure
plot(Smodel(329:333,1),NAT(329:333,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('5/25/2014')

figure
plot(Smodel(354:358,1),NAT(354:358,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('7/14/2014')

figure
plot(Smodel(379:383,1),NAT(379:383,3),'o','MarkerSize',marksize); hold on
ylabel('observed salinity')
xlabel('modeled salinity')
line([0 100],[0 100],'Color','k','LineStyle','-')
title('10/17/2014')
%% model vs. observations: same class, +/- elevaiton, all model salinities
% clear
% 
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
%     'vegetation_class_complete','nu','centroids')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
% NAT(:,7) = NAT(:,7) + 0.204;
% observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];
% 
% % cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ndm20_diff5cm_drainage')
% % cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/field90/')
% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/field0/')
% 
% % observation class index
% Juncus = vertcat((1:5)',(24:28)',(49:53)',(74:78)',(99:103)',(124:128)',(149:153)',(174:178)',(199:203)',(224:228)',(249:253)',(274:278)',(299:303)',(324:328)',(349:353)',(374:378)');
% MarshMeadow = vertcat((6:10)',(29:33)',(54:58)',(79:83)',(104:108)',(129:133)',(154:158)',(179:183)',(204:208)',(229:233)',(254:258)',(279:283)',(304:308)',(329:333)',(354:358)',(379:383)');
% MedSpart = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)');
% ShortSpart = vertcat((14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)');
% TallSpart = vertcat((19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');
% 
% Spartina = vertcat((11:13)',(34:38)',(59:63)',(84:88)',(109:113)',(134:138)',(159:163)',(184:188)',(209:213)',(234:238)',(259:263)',(284:288)',(309:313)',(334:338)',(359:363)',(384:388)',...
%     (14:18)',(39:43)',(64:68)',(89:93)',(114:118)',(139:143)',(164:168)',(189:193)',(214:218)',(239:243)',(264:268)',(289:293)',(314:318)',(339:343)',(364:368)',(389:393)',...
%     (19:23)',(44:48)',(69:73)',(94:98)',(119:123)',(144:148)',(169:173)',(194:198)',(219:223)',(244:248)',(269:273)',(294:298)',(319:323)',(344:348)',(369:373)',(394:398)');
% 
% dz = eps;
% 
% % model index
% for i = [3 6 7 8 1]
%     clear vindex elmt
%     vindex = find(vegetation_class_complete == i);
%     if i == 3
%         for j = 1 : length(Juncus)
%             [elmt] = abs(centroids(vindex,3)-NAT(Juncus(j,1),7)) <= dz;
%             jrindex{j} = vindex(elmt);
%         end
%     end
%     if i == 6
%         for j = 1 : length(MedSpart)
%             [elmt] = abs(centroids(vindex,3)-NAT(MedSpart(j,1),7)) <= dz;
%             msindex{j} = vindex(elmt);
%         end
%     end
%     if i == 7
%         for j = 1 : length(ShortSpart)
%             [elmt] = abs(centroids(vindex,3)-NAT(ShortSpart(j,1),7)) <= dz;
%             ssindex{j} = vindex(elmt);
%         end
%     end
%     if i == 8
%         for j = 1 : length(TallSpart)
%             [elmt] = abs(centroids(vindex,3)-NAT(TallSpart(j,1),7)) <= dz;
%             tsindex{j} = vindex(elmt);
%         end
%     end
%     if i == 1
%         for j = 1 : length(MarshMeadow)
%             [elmt] = abs(centroids(vindex,3)-NAT(MarshMeadow(j,1),7)) <= dz;
%             mmindex{j} = vindex(elmt);
%         end
%     end
% end
% 
% ind(Juncus,1) = 3;
% ind(MarshMeadow,1) = 9;
% ind(MedSpart,1) = 6;
% ind(ShortSpart,1) = 7;
% ind(TallSpart,1) = 8;
% 
% dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021];
% dayobs(1:23) = dayrecord(1);
% dayobs(24:48) = dayrecord(2);
% dayobs(49:73) = dayrecord(3);
% dayobs(74:98) = dayrecord(4);
% dayobs(99:123) = dayrecord(5);
% dayobs(124:148) = dayrecord(6);
% dayobs(149:173) = dayrecord(7);
% dayobs(174:198) = dayrecord(8);
% dayobs(199:223) = dayrecord(9);
% dayobs(224:248) = dayrecord(10);
% dayobs(249:273) = dayrecord(11);
% dayobs(274:298) = dayrecord(12);
% dayobs(299:323) = dayrecord(13);
% dayobs(324:348) = dayrecord(14);
% dayobs(349:373) = dayrecord(15);
% dayobs(374:398) = dayrecord(16);
% dayobs = dayobs(:);
% a = 0;
% b = 0;
% c = 0;
% d = 0;
% e = 0;
% 
% for i = 1 : length(dayobs)
%     filename = strcat('day',num2str(dayobs(i)),'.mat');
%     load(filename,'S1')
%     if ind(i) == 3
%         a = a + 1;
%         Smodeljr{a} = S1(1,jrindex{a}(:,1));
%         Sobsjr{a} = NAT(i,3) * ones(length(Smodeljr{a}),1);
%     end
%     if ind(i) == 6
%         b = b + 1;
%         Smodelms{b} = S1(1,msindex{b}(:,1));
%         Sobsms{b} = NAT(i,3) * ones(length(Smodelms{b}),1);
%    end
%     if ind(i) == 7
%         c = c + 1;
%         Smodelss{c} = S1(1,ssindex{c}(:,1));
%         Sobsss{c} = NAT(i,3) * ones(length(Smodelss{c}),1);
%    end
%     if ind(i) == 8
%         d = d + 1;
%         Smodelts{d} = S1(1,tsindex{d}(:,1));
%         Sobsts{d} = NAT(i,3) * ones(length(Smodelts{d}),1);
%     end
%     if ind(i) == 9
%         e = e + 1;
%         Smodelmm{e} = S1(1,mmindex{e}(:,1));
%         Sobsmm{e} = NAT(i,3) * ones(length(Smodelmm{e}),1);
% %         Smodel(i,1) = nan;
% %         Smodel(i,2) = nan;
%      end
% end
% 
% figure
% for i = 1 : length(tsindex)
%     plot(Smodelts{i},Sobsts{i},'.b'); hold on
% end
% for i = 1 : length(msindex)
%     plot(Smodelms{i},Sobsms{i},'.r'); hold on
% end
% for i = 1 : length(ssindex)
%     plot(Smodelss{i},Sobsss{i},'.g'); hold on
% end
% for i = 1 : length(jrindex)
%     plot(Smodeljr{i},Sobsjr{i},'.c'); hold on
% end
% for i = 1 : length(mmindex)
%     plot(Smodelmm{i},Sobsmm{i},'.m'); hold on
% end
% 
% ylabel('observed salinity')
% xlabel('modeled salinity')
% line([0 100],[0 100],'Color','k','LineStyle','-')
% % indgamma = ~isnan(Smodel(:,1));
% % gamma = NAT(indgamma,3)\Smodel(indgamma,1);
% % gamma = Smodel(:,1)\NAT(:,3);
% 
% % plot(0:100,(0:100)/gamma,'k--')
% grid on
% grid minor
% axis square
% axis equal
% 
% xlim([0 100])
% ylim([0 100])
% % xlim([0 60])
% % ylim([0 60])
% legend('TS','MS','SS','JR','SV','1:1','least-squares','Location','East')
% title(['same class, all points +/- elevation dz = ' num2str(dz) ' m'])
%% model vs. observations: elevation difference, salinity difference
% clear
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/duplinObservations.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/unsmoothedNDM20_OutofRangeElements.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/NDM20_3years.mat',...
    'vegetation_class_complete','nu','centroids')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/validation/GCE10pointsNDM20.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/validation_area.mat')
NAT(:,7) = NAT(:,7) + 0.204;
observComp = [GIS(:,1:6); NAT(:,1:6); PORGCE10(:,1:6)];

cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ET75_MM125_dz1/baseline')

figure
% load('unsmoothedNDM20_OutofRangeElements.mat')
% for j = 1 : 16
%     if j == 1; interval = 1:23; end
%     if j == 2; interval = 24:48; end
%     if j == 3; interval = 49:73; end
%     if j == 4; interval = 74:98; end
%     if j == 5; interval = 99:123; end
%     if j == 6; interval = 124:148; end
%     if j == 7; interval = 149:173; end
%     if j == 8; interval = 174:198; end
%     if j == 9; interval = 199:223; end
%     if j == 10; interval = 224:248; end
%     if j == 11; interval = 249:273; end
%     if j == 12; interval = 274:298; end
%     if j == 13; interval = 299:323; end
%     if j == 14; interval = 324:348; end
%     if j == 15; interval = 349:373; end
%     if j == 16; interval = 374:398; end
    
    points_in_region = elements_in_domain_valid;
    
    % Load data
dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021]+366;

    dayobs(1:23) = dayrecord(1);
    dayobs(24:48) = dayrecord(2);
    dayobs(49:73) = dayrecord(3);
    dayobs(74:98) = dayrecord(4);
    dayobs(99:123) = dayrecord(5);
    dayobs(124:148) = dayrecord(6);
    dayobs(149:173) = dayrecord(7);
    dayobs(174:198) = dayrecord(8);
    dayobs(199:223) = dayrecord(9);
    dayobs(224:248) = dayrecord(10);
    dayobs(249:273) = dayrecord(11);
    dayobs(274:298) = dayrecord(12);
    dayobs(299:323) = dayrecord(13);
    dayobs(324:348) = dayrecord(14);
    dayobs(349:373) = dayrecord(15);
    dayobs(374:398) = dayrecord(16);
    dayobs = dayobs(:);
    
    
    % find nearest element to observation locations
    for i = 1 : length(NAT(interval))
        x_dist = centroids(:,1)-NAT(i,1);
        y_dist = centroids(:,2)-NAT(i,2);
        dist = sqrt(x_dist.^2+y_dist.^2);
        [a,b] = min(dist);
        
        ind_dist(i,1) = b; % index
        ind_dist(i,2) = a; % distance (m)
    end
    
    for i = 1 : length(dayobs(interval))
        filename = strcat('day',num2str(dayobs(i)),'.mat');
        load(filename,'S1')
        Smodel(i,1) = S1(ind_dist(i,1));
    end
    
    % figure
    % subplot(4,4,j)
    % plot(Smodel(:,1),NAT(interval,3),'.b')
    % ylabel('observed')
    % xlabel('modeled')
    % title([num2str(NAT(interval(1),5)) '/' num2str(NAT(interval(1),4)) '/' num2str(NAT(interval(1),6))])
    % line([0 100],[0 100])
    
    zdiff = NAT(interval,7) - centroids(ind_dist(:,1),3);
    sdiff = NAT(interval,3) - Smodel;
    
    subplot(4,4,j)
    plot(zdiff,sdiff,'.')
    xlabel('z difference (m)')
    ylabel('salinity difference')
    title([num2str(NAT(interval(1),5)) '/' num2str(NAT(interval(1),4)) '/' num2str(NAT(interval(1),6))])
    
    grid on
    grid minor
% end


%% PLOT SALINITY FIELDS
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/NDM20_3years.mat')
% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/ndm20_diff5cm_drainage')
% dayrecord = [171;198;269;280;315;351;385;420;454;520;563;641;804;877;927;1021];
% 
% % dayrecord = [1;32;61;92;122;153;183;214;245;275;306;336;367;398;426;457;487;...
% %     518;548;579;610;640;671;701;732;763;791;822;852;883;913;944;975;...
% %     1005;1036;1066;1096];
% 
% j = 2;
% 
% for i = 1 : length(dayrecord)
%     filename = strcat('day',num2str(dayrecord(i)),'.mat');
%     load(filename,'S1')
%     eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
% end
% 
% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/MOBJ')
% load('NDM20_unsmoothed.mat')
% load('NDM20_unsmoothed.mat','centroids','nodes_variable','vegetation_class_complete')
% Mobj = read_sms_mesh_dd( '2dm', 'NDM20.2dm', 'bath', 'NDM20.pts' );
% Mobj = setup_metrics(Mobj);

% Sinterp = scatteredInterpolant( centroids(:,1), centroids(:,2), eval(strcat('Sday',num2str(dayrecord(j)),'(1,:)'))');
Sinterp = scatteredInterpolant( centroids(:,1), centroids(:,2), centroids(:,3));
salinity(1,:) = Sinterp(nodes(:,1), nodes(:,2));
% titlename = strcat('salinity day',num2str(dayrecord(j)));
titlename = strcat('elevation')
figure
plot_field(Mobj,salinity,'title',titlename);
axis equal
colormap jet
colorbar;
% caxis([0 70]);
axis image
xlimits = [4.7301e+05, 4.755e+05]; % GCE10
ylimits = [3.4820e+06, 3.4844e+06]; % GCE10

xlim(xlimits)
ylim(ylimits)

hold on
% plot(NAT(:,1),NAT(:,2),'K.')
for i = 1 : length(tsindex)
    plot(centroids(tsindex{i},1),centroids(tsindex{i},2),'.b'); hold on
end
for i = 1 : length(msindex)
    plot(centroids(msindex{i},1),centroids(msindex{i},2),'.k'); hold on
end
for i = 1 : length(ssindex)
    plot(centroids(ssindex{i},1),centroids(ssindex{i},2),'.g'); hold on
end
for i = 1 : length(jrindex)
    plot(centroids(jrindex{i},1),centroids(jrindex{i},2),'.c'); hold on
end
for i = 1 : length(mmindex)
    plot(centroids(mmindex{i},1),centroids(mmindex{i},2),'.m'); hold on
end
