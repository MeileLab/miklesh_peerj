%% sensitivity analysis - annual


%% all classes, all parameters, timeseries structure
tic
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')

vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
    'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
    'S. virginica';'shell';'open water';'upland'};

% first day of each month for 2014 and december 31,2014
dayrecord = (732:1095) + 365;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095] ;

cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/baseline')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
    end
    
end
field = 'base';
value = {meanS;lowS;highS;medS};
b = struct(field,value);


cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/et')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field1 = 'et';
value1 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};

cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/precipitation')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field2 = 'rain';
value2 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};

cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/salinity')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field3 = 'tide';
value3 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};


cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/saltexchange')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field4 = 'diff';
value4 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};

cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/ks')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field5 = 'ks';
value5 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};

cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/slr')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field6 = 'slr';
value6 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};
cd('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/fc99')
for i = 1 : length(dayrecord)
    filename = strcat('day',num2str(dayrecord(i)),'.mat');
    load(filename,'S1')
    for j = 1 : 12
        %         I = find(vegetation_class_complete == j);
        VegIndex = (vegetation_class_complete == j);
        OutIndex = logical(ones(length(vegetation_class_complete),1));
        OutIndex(outofbounds,1) = logical(0);
        GCE10Index(GCE10pointsNDM20,1) = logical(1);
        Index = VegIndex .* OutIndex .* GCE10Index;
        Index = logical(Index);
        
        meanS(i,j) = mean(S1(1,Index));
        lowS(i,j) = prctile(S1(1,Index),25);
        highS(i,j) = prctile(S1(1,Index),75);
        medS(i,j) = median(S1(1,Index));
        Pmean(i,j) = 100*(meanS(i,j)-b(1).base(i,j))/b(1).base(i,j);
        Phigh(i,j) = 100*(highS(i,j)-b(3).base(i,j))/b(3).base(i,j);
        Plow(i,j) = 100*(lowS(i,j)-b(2).base(i,j))/b(2).base(i,j);
        Pmed(i,j) = 100*(medS(i,j)-b(4).base(i,j))/b(4).base(i,j);
    end
    
end
field7 = 'fc99';
value7 = {meanS;lowS;highS;medS;Pmean;Plow;Phigh;Pmed};
s = struct(field1,value1,field2,value2,field3,value3,field4,value4,field5,value5,field6,value6,field7,value7);
toc

%% one param, selected classes, median with ET and Altamaha

% figure properties
textsize = 12;
textsizetitle = 16;
linewidth1 = 3;
linewidth2 = 1;
linewidth3 = 3;

load('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/sensitivity_Rs70_a10_JRMSsame_fc99.mat'); % data from all sims
% load('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/sensitivity_Rs70_a10_JRMSsame_fc94.mat'); % data from all sims
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/annualfigurevariables.mat','raindays')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/doctQ4fig.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/NDM20_Rs70_a10.mat','ETdayRs70_a10_4year','rain','salinity')

ETday = ETdayRs70_a10_4year;

    for i = 1 : 1460
        % define indices
        i1= 24*i-23;
        i2= 24*i;
        
        % precipitation data
        rainday(i)=sum(rain(i1:i2));
    end


vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
    'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
    'Sarcocornia sp.';'shell';'open water';'upland'};

% first day of each month for 2014 and december 31,2014
dayrecord = 732:1095;
temp = [731;763;791;822;852;883;913;944;975;1005;1036;1066]+365;
params = {'ET +10%','precipitation +10%','river salinity +10%','salt exchange x2','conductivity x10','SLR +5 cm','f_{c} - .024'};
strcname = {'et','rain','tide','diff','ks','slr','fc99'};

figure('Color',[1 1 1],'Name','median')
for i = 1 : 7
    subplot(9,1,i)
    for j = [6,3]
        %         ylimit = [-15 15];
        n=1;
        gcs = conv(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
        if j == 6
            h = plot(gcs,'k-',...
                'LineWidth',linewidth1,...
                'Color','k'); hold on
            ylabel('% \Delta')
            txt = char(str2mat(params(i)));
            h = title(txt,'FontSize',textsizetitle);
            set(h,'HorizontalAlignment','center');
            
            set(gca,...
                'FontWeight','bold',...
                'FontSize',textsize,...
                'XLim',[4 360],...
                'TickLength',[0 0],...
                'XTick',zeros(1,0),...
                'XColor','k',...
                'YColor','k',...
                'XMinorGrid','off',...
                'YMinorGrid','on',...
                'YGrid','on'),...
                %             'YLim',[-50 15],...
            %             'YTick',[-45 -30 -15 0 15])
        else
            h = plot(gcs,'k-',...
                'LineWidth',linewidth1,...
                'Color',[0.6 0.6 0.6]); hold on
            ylabel('% \Delta')
            txt = char(str2mat(params(i)));
            h = title(txt,'FontSize',textsizetitle);
            set(h,'HorizontalAlignment','center');
            
            set(gca,...
                'FontWeight','bold',...
                'FontSize',textsize,...
                'XLim',[4 360],...
                'TickLength',[0 0],...
                'XTick',zeros(1,0),...
                'XColor','k',...
                'YColor','k',...
                'XMinorGrid','off',...
                'YMinorGrid','on',...
                'YGrid','on')%,...
            %             'YLim',[-50 15],...
            %             'YTick',[-45 -30 -15 0 15])
        end
        if i == 1
            legend(vege_name{6},vege_name{3},'Location','NorthWest');
            set(gca,...
                'YLim',[0 15]);
        end
        if i == 2
            set(gca,...
                'YLim',[-8 0]);
        end
        if i == 3
            set(gca,...
                'YLim',[9 11]);
        end
        if i == 4
            set(gca,...
                'YLim',[-13 4]);
        end
        if i == 5
            set(gca,...
                'YLim',[-30 10],...
                'YTick',[-30 -20 -10 0 10]);
        end
        if i == 6
            set(gca,...
                'YLim',[-10 10]);
        end
        if i == 7
            set(gca,...
                'YLim',[-1 2]);
        end
    end
end

subplot(9,1,8)
% gcs = conv(salinity(1:26304), 1/n*ones(1,n), 'same');
% [hAx,hLine1,hLine2] = plotyy(17568:26304,gcs(17568:26304),(doctQ4fig(668:1032,2)-13)*24-23,doctQ4fig(668:1032,3)); 
gcs = conv(salinity(26304:35064), 1/n*ones(1,n), 'same');
[hAx,hLine1,hLine2] = plotyy(17545:26304,gcs(1:8760),(doctQ4fig(668:1032,2)-13)*24-23,doctQ4fig(668:1032,3)); 

ylabel(hAx(1),{'Duplin River';'salinity'})
ylabel(hAx(2),{'Altamaha River';'Discharge (m^3 day^-^1)'},'FontWeight','bold','FontSize',textsize)

set(gca,...
    'FontSize',textsize,...
    'FontWeight','Bold',...
    'XLim',[17568 26304],...
     'XTickLabel','',...
    'TickLength',[0 0],...
    'YLim',[10 35],...
    'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XMinorGrid','off',...
    'YMinorGrid','on',...
    'XColor','k',...
    'YColor','k')
set(hAx(2),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[17568 26304],...
    'YLim',[0 1500],...
    'YTick',[0 500 1000 1500],...
    'XColor',[0.4 0.4 0.4],...
    'YColor',[0.4 0.4 0.4])
set(hAx(1),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[17568 26304],...
    'XColor','k',...
    'YColor','k')
set(hLine1,...
    'LineWidth',linewidth1,...
    'Color','k')
set(hLine2,...
    'LineWidth',linewidth1,...
    'Color',[0.4 0.4 0.4])

subplot(9,1,9)
% [hAx,hLine1,hLine2] = plotyy(730:1096, ETday(6,730:1096)*1000,730:1096,rainday(730:1096)*100); hold on    % ,'-k','LineWidth',2); 
[hAx,hLine1,hLine2] = plotyy(1096:1460, ETday(6,1096:1460)*1000,1096:1460,rainday(1,1096:1460)*100); hold on    % ,'-k','LineWidth',2); 

ylabel(hAx(1),'ET (mm day^-^1)','FontWeight','bold','FontSize',textsize)
% ylabel(hAx(2),'rain (mm day^-^1)','FontWeight','bold','FontSize',textsize)
ylabel(hAx(2),'rain (cm day^-^1)','FontWeight','bold','FontSize',textsize)
set(gca,...
    'FontSize',textsize,...
    'FontWeight','Bold',...
    'XLim',[1096 1460],...
    'TickLength',[0 0],...
    'XTick',temp,...
    'YLim',[0 5],...
    'YTick',[0 1 2 3 4 5],...
    'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'},...
    'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XMinorGrid','off',...
    'YMinorGrid','on',...
    'XColor','k',...
    'YColor','k')
set(hAx(2),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[1096 1460],...
    'XColor','k',...
    'YColor',[0.4 0.4 0.4])
set(hAx(1),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[1096 1460],...
    'XColor','k',...
    'YColor','k')
set(hLine1,...
    'LineWidth',linewidth3,...
    'Color','k')
set(hLine2,...
    'LineWidth',linewidth3,...
    'Color',[0.4 0.4 0.4])

%% one param, selected classes, mean with ET and Altamaha
%% one param, selected classes, median with ET and Altamaha

% figure properties
textsize = 12;
textsizetitle = 16;
linewidth1 = 3;
linewidth2 = 1;
linewidth3 = 3;

% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
load('/Users/david/Dropbox/school/research/soil model/simulations/4_year_outputs/sensitivity/sensitivity_Rs70_a10_JRMSsame.mat'); % data from all sims
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/annualfigurevariables.mat','raindays')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/annual/doctQ4fig.mat')
load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/NDM20_Rs70_a10.mat','ETdayRs70_a10_4year','rain','salinity')

ETday = ETdayRs70_a10_4year;

%     for i = 1 : 1460
%         % define indices
%         i1= 24*i-23;
%         i2= 24*i;
%         
%         % precipitation data
%         rainday(j,i)=sum(rain(j,i1:i2));
%     end


vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
    'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
    'Sarcocornia sp.';'shell';'open water';'upland'};

% first day of each month for 2014 and december 31,2014
dayrecord = 732:1095;
temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066];
params = {'ET +10%','precipitation +10%','river salinity +10%','salt exchange x2','conductivity x10','SLR +5 cm','f_{c} + .024'};
strcname = {'et','rain','tide','diff','ks','slr','fc99'};

figure('Color',[1 1 1],'Name','median')
for i = 1 : 7
    subplot(9,1,i)
    for j = [6,3]
        %         ylimit = [-15 15];
        n=1;
        gcs = conv(eval(str2mat(strcat('s(5).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
        if j == 6
            h = plot(gcs,'k-',...
                'LineWidth',linewidth1,...
                'Color','k'); hold on
            ylabel('% \Delta')
            txt = char(str2mat(params(i)));
            h = title(txt,'FontSize',textsizetitle);
            set(h,'HorizontalAlignment','center');
            
            set(gca,...
                'FontWeight','bold',...
                'FontSize',textsize,...
                'XLim',[4 360],...
                'TickLength',[0 0],...
                'XTick',zeros(1,0),...
                'XColor','k',...
                'YColor','k',...
                'XMinorGrid','off',...
                'YMinorGrid','on',...
                'YGrid','on'),...
                %             'YLim',[-50 15],...
            %             'YTick',[-45 -30 -15 0 15])
        else
            h = plot(gcs,'k-',...
                'LineWidth',linewidth1,...
                'Color',[0.6 0.6 0.6]); hold on
            ylabel('% \Delta')
            txt = char(str2mat(params(i)));
            h = title(txt,'FontSize',textsizetitle);
            set(h,'HorizontalAlignment','center');
            
            set(gca,...
                'FontWeight','bold',...
                'FontSize',textsize,...
                'XLim',[4 360],...
                'TickLength',[0 0],...
                'XTick',zeros(1,0),...
                'XColor','k',...
                'YColor','k',...
                'XMinorGrid','off',...
                'YMinorGrid','on',...
                'YGrid','on')%,...
            %             'YLim',[-50 15],...
            %             'YTick',[-45 -30 -15 0 15])
        end
        if i == 1
            legend(vege_name{6},vege_name{3},'Location','NorthWest');
            set(gca,...
                'YLim',[0 15]);
        end
        if i == 2
            set(gca,...
                'YLim',[-8 0]);
        end
        if i == 3
            set(gca,...
                'YLim',[9 11]);
        end
        if i == 4
            set(gca,...
                'YLim',[-13 4]);
        end
        if i == 5
            set(gca,...
                'YLim',[-30 10],...
                'YTick',[-30 -20 -10 0 10]);
        end
        if i == 6
            set(gca,...
                'YLim',[-10 10]);
        end
        if i == 7
            set(gca,...
                'YLim',[-1 2]);
        end
    end
end
subplot(9,1,8)
gcs = conv(salinity(1:26304), 1/n*ones(1,n), 'same');
[hAx,hLine1,hLine2] = plotyy(17568:26304,gcs(17568:26304),(doctQ4fig(668:1032,2)-13)*24-23,doctQ4fig(668:1032,3)); 

ylabel(hAx(1),{'Duplin River';'salinity'})
ylabel(hAx(2),{'Altamaha River';'Discharge (m^3 day^-^1)'},'FontWeight','bold','FontSize',textsize)

set(gca,...
    'FontSize',textsize,...
    'FontWeight','Bold',...
    'XLim',[17568 26304],...
     'XTickLabel','',...
    'TickLength',[0 0],...
    'YLim',[0 40],...
    'YTick',[0 10 20 30 40],...
    'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XMinorGrid','off',...
    'YMinorGrid','on',...
    'XColor','k',...
    'YColor','k')
set(hAx(2),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[17568 26304],...
    'YLim',[0 1500],...
    'YTick',[0 500 1000 1500],...
    'XColor',[0.4 0.4 0.4],...
    'YColor',[0.4 0.4 0.4])
set(hAx(1),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[17568 26304],...
    'XColor','k',...
    'YColor','k')
set(hLine1,...
    'LineWidth',linewidth1,...
    'Color','k')
set(hLine2,...
    'LineWidth',linewidth1,...
    'Color',[0.4 0.4 0.4])

subplot(9,1,9)
[hAx,hLine1,hLine2] = plotyy(730:1096, ETday(6,730:1096)*1000,730:1096,raindays(730:1096)*100); hold on    % ,'-k','LineWidth',2); 

ylabel(hAx(1),'ET (mm day^-^1)','FontWeight','bold','FontSize',textsize)
% ylabel(hAx(2),'rain (mm day^-^1)','FontWeight','bold','FontSize',textsize)
ylabel(hAx(2),'rain (cm day^-^1)','FontWeight','bold','FontSize',textsize)
set(gca,...
    'FontSize',textsize,...
    'FontWeight','Bold',...
    'XLim',[730 1096],...
    'TickLength',[0 0],...
    'XTick',temp,...
    'YLim',[0 6],...
    'YTick',[0 2 4 6],...
    'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'},...
    'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XMinorGrid','off',...
    'YMinorGrid','on',...
    'XColor','k',...
    'YColor','k')
set(hAx(2),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[730 1096],...
    'XColor','k',...
    'YColor',[0.4 0.4 0.4])
set(hAx(1),...
    'FontWeight','bold',...
    'FontSize',textsize,...
    'XLim',[730 1096],...
    'XColor','k',...
    'YColor','k')
set(hLine1,...
    'LineWidth',linewidth3,...
    'Color','k')
set(hLine2,...
    'LineWidth',linewidth3,...
    'Color',[0.4 0.4 0.4])


%% one param, selected classes, median
% 
% % figure properties
% textsize = 12;
% textsizetitle = 16;
% linewidth1 = 3;
% linewidth2 = 1;
% linewidth3 = 3;
% 
% % load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/annualfigurevariables.mat','raindays')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'Sarcocornia sp.';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET +10%','precipitation +10%','river salinity +10%','salt exchange x2','conductivity x10','SLR +5 cm','f_{c} = .99'};
% strcname = {'et','rain','tide','diff','ks','slr','fc99'};
% 
% figure('Color',[1 1 1],'Name','mean')
% for i = 1 : 7
%     subplot(8,1,i)
%     for j = [6,3]
%         %         ylimit = [-15 15];
%         n=1;
%         gcs = conv(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
%         if j == 6
%             h = plot(gcs,'k-',...
%                 'LineWidth',linewidth1,...
%                 'Color','k'); hold on
%             ylabel('% \Delta')
%             txt = char(str2mat(params(i)));
%             h = title(txt,'FontSize',textsizetitle);
%             set(h,'HorizontalAlignment','center');
%             
%             set(gca,...
%                 'FontWeight','bold',...
%                 'FontSize',textsize,...
%                 'XLim',[4 360],...
%                 'TickLength',[0 0],...
%                 'XTick',zeros(1,0),...
%                 'XColor','k',...
%                 'YColor','k',...
%                 'XMinorGrid','off',...
%                 'YMinorGrid','on',...
%                 'YGrid','on'),...
%                 %             'YLim',[-50 15],...
%             %             'YTick',[-45 -30 -15 0 15])
%         else
%             h = plot(gcs,'k-',...
%                 'LineWidth',linewidth1,...
%                 'Color',[0.6 0.6 0.6]); hold on
%             ylabel('% \Delta')
%             txt = char(str2mat(params(i)));
%             h = title(txt,'FontSize',textsizetitle);
%             set(h,'HorizontalAlignment','center');
%             
%             set(gca,...
%                 'FontWeight','bold',...
%                 'FontSize',textsize,...
%                 'XLim',[4 360],...
%                 'TickLength',[0 0],...
%                 'XTick',zeros(1,0),...
%                 'XColor','k',...
%                 'YColor','k',...
%                 'XMinorGrid','off',...
%                 'YMinorGrid','on',...
%                 'YGrid','on')%,...
%             %             'YLim',[-50 15],...
%             %             'YTick',[-45 -30 -15 0 15])
%         end
%         if i == 1
%             legend(vege_name{6},vege_name{3},'Location','NorthWest');
%             set(gca,...
%                 'YLim',[0 15]);
%         end
%         if i == 2
%             set(gca,...
%                 'YLim',[-10 0]);
%         end
%         if i == 3
%             set(gca,...
%                 'YLim',[9 12]);
%         end
%         if i == 4
%             set(gca,...
%                 'YLim',[-10 0]);
%         end
%         if i == 5
%             set(gca,...
%                 'YLim',[-10 10]);
%         end
%         if i == 6
%             set(gca,...
%                 'YLim',[-10 10]);
%         end
%         if i == 7
%             set(gca,...
%                 'YLim',[-5 5]);
%         end
%     end
% end
% subplot(8,1,8)
% linewidth1 = 6;
% linewidth2 = 3;
% gcs = conv(S(1:26304), 1/n*ones(1,n), 'same');
% % [hAx,hLine1,hLine2] = plotyy(17568:26300,gcs(17568:26300),17568:26300,rain(17568:26300)*100);
% 
% [hAx,hLine1,hLine2] = plotyy(dayrecord*24,S(dayrecord*24),dayrecord*24,raindays(dayrecord)*100);
% ylabel(hAx(1),{'Duplin River';'salinity'}) % left y-axis
% ylabel(hAx(2),'rain (cm day^-^1)','FontWeight','bold','FontSize',textsize) % right y-axis
% % set(hAx,'FontSize',textsize,'FontWeight','Bold','XLim',[17568 26304],'TickLength',[0 0])
% % hAx = gca;
% % hAx.FontWeight = 'bold';
% % hAx.FontSize = textsize;
% % hAx.XTick = temp*24;
% % % hAx.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
% % hAx.YGrid = 'on';
% % set(gca,'FontWeight','bold','FontSize',textsize)
% % hLine1.LineWidth = linewidth1;
% % hLine2.LineWidth = linewidth2;
% % hLine1.Marker = '.';
% % hAx.YTick = [0 10 20 30 40];
% % ylim=[0 40];
% % grid minor
% 
% set(gca,...
%     'FontSize',textsize,...
%     'FontWeight','Bold',...
%     'XLim',[17568 26304],...
%     'TickLength',[0 0],...
%     'XTick',temp*24,...
%     'YLim',[0 40],...
%     'YTick',[0 10 20 30 40],...
%     'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'},...
%     'YGrid','on',...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XMinorGrid','off',...
%     'YMinorGrid','on',...
%     'XColor','k',...
%     'YColor','k')
% set(hAx(2),...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XLim',[17568 26304],...
%     'XColor','k',...
%     'YColor',[0.4 0.4 0.4])
% set(hAx(1),...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XLim',[17568 26304],...
%     'XColor','k',...
%     'YColor','k')
% set(hLine1,...
%     'LineWidth',linewidth2,...
%     'Color','k')
% set(hLine2,...
%     'LineWidth',linewidth3,...
%     'Color',[0.4 0.4 0.4])

%% one param, selected classes, mean
% 
% % figure properties
% textsize = 12;
% textsizetitle = 16;
% linewidth1 = 3;
% linewidth2 = 1;
% linewidth3 = 3;
% 
% % load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/annualfigurevariables.mat','raindays')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'Sarcocornia sp.';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET +10%','precipitation +10%','river salinity +10%','salt exchange x2','conductivity x10','SLR +5 cm','f_{c} = .99'};
% strcname = {'et','rain','tide','diff','ks','slr','fc99'};
% 
% figure('Color',[1 1 1],'Name','mean')
% for i = 1 : 7
%     subplot(8,1,i)
%     for j = [6,3]
%         %         ylimit = [-15 15];
%         n=1;
%         gcs = conv(eval(str2mat(strcat('s(5).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
%         if j == 6
%             h = plot(gcs,'k-',...
%                 'LineWidth',linewidth1,...
%                 'Color','k'); hold on
%             ylabel('% \Delta')
%             txt = char(str2mat(params(i)));
%             h = title(txt,'FontSize',textsizetitle);
%             set(h,'HorizontalAlignment','center');
%             
%             set(gca,...
%                 'FontWeight','bold',...
%                 'FontSize',textsize,...
%                 'XLim',[4 360],...
%                 'TickLength',[0 0],...
%                 'XTick',zeros(1,0),...
%                 'XColor','k',...
%                 'YColor','k',...
%                 'XMinorGrid','off',...
%                 'YMinorGrid','on',...
%                 'YGrid','on'),...
%                 %             'YLim',[-50 15],...
%             %             'YTick',[-45 -30 -15 0 15])
%         else
%             h = plot(gcs,'k-',...
%                 'LineWidth',linewidth1,...
%                 'Color',[0.6 0.6 0.6]); hold on
%             ylabel('% \Delta')
%             txt = char(str2mat(params(i)));
%             h = title(txt,'FontSize',textsizetitle);
%             set(h,'HorizontalAlignment','center');
%             
%             set(gca,...
%                 'FontWeight','bold',...
%                 'FontSize',textsize,...
%                 'XLim',[4 360],...
%                 'TickLength',[0 0],...
%                 'XTick',zeros(1,0),...
%                 'XColor','k',...
%                 'YColor','k',...
%                 'XMinorGrid','off',...
%                 'YMinorGrid','on',...
%                 'YGrid','on')%,...
%             %             'YLim',[-50 15],...
%             %             'YTick',[-45 -30 -15 0 15])
%         end
%         if i == 1
%             legend(vege_name{6},vege_name{3},'Location','NorthWest');
%             set(gca,...
%                 'YLim',[0 15]);
%         end
%         if i == 2
%             set(gca,...
%                 'YLim',[-10 0]);
%         end
%         if i == 3
%             set(gca,...
%                 'YLim',[9 12]);
%         end
%         if i == 4
%             set(gca,...
%                 'YLim',[-10 0]);
%         end
%         if i == 5
%             set(gca,...
%                 'YLim',[-10 10]);
%         end
%         if i == 6
%             set(gca,...
%                 'YLim',[-10 10]);
%         end
%         if i == 7
%             set(gca,...
%                 'YLim',[-5 5]);
%         end
%     end
% end
% subplot(8,1,8)
% linewidth1 = 6;
% linewidth2 = 3;
% gcs = conv(S(1:26304), 1/n*ones(1,n), 'same');
% % [hAx,hLine1,hLine2] = plotyy(17568:26300,gcs(17568:26300),17568:26300,rain(17568:26300)*100);
% 
% [hAx,hLine1,hLine2] = plotyy(dayrecord*24,S(dayrecord*24),dayrecord*24,raindays(dayrecord)*100);
% ylabel(hAx(1),{'Duplin River';'salinity'}) % left y-axis
% ylabel(hAx(2),'rain (cm day^-^1)','FontWeight','bold','FontSize',textsize) % right y-axis
% % set(hAx,'FontSize',textsize,'FontWeight','Bold','XLim',[17568 26304],'TickLength',[0 0])
% % hAx = gca;
% % hAx.FontWeight = 'bold';
% % hAx.FontSize = textsize;
% % hAx.XTick = temp*24;
% % % hAx.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
% % hAx.YGrid = 'on';
% % set(gca,'FontWeight','bold','FontSize',textsize)
% % hLine1.LineWidth = linewidth1;
% % hLine2.LineWidth = linewidth2;
% % hLine1.Marker = '.';
% % hAx.YTick = [0 10 20 30 40];
% % ylim=[0 40];
% % grid minor
% 
% set(gca,...
%     'FontSize',textsize,...
%     'FontWeight','Bold',...
%     'XLim',[17568 26304],...
%     'TickLength',[0 0],...
%     'XTick',temp*24,...
%     'YLim',[0 40],...
%     'YTick',[0 10 20 30 40],...
%     'XTickLabel',{'J','F','M','A','M','J','J','A','S','O','N','D'},...
%     'YGrid','on',...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XMinorGrid','off',...
%     'YMinorGrid','on',...
%     'XColor','k',...
%     'YColor','k')
% set(hAx(2),...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XLim',[17568 26304],...
%     'XColor','k',...
%     'YColor',[0.4 0.4 0.4])
% set(hAx(1),...
%     'FontWeight','bold',...
%     'FontSize',textsize,...
%     'XLim',[17568 26304],...
%     'XColor','k',...
%     'YColor','k')
% set(hLine1,...
%     'LineWidth',linewidth2,...
%     'Color','k')
% set(hLine2,...
%     'LineWidth',linewidth3,...
%     'Color',[0.4 0.4 0.4])
% %% plot all-structure - median
% 
% % figure properties
% textsize = 14;
% textsizetitle = 16;
% 
% % load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'S. virginica';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET','rain','river salinity','salt exchange','Ks','SLR'};
% strcname = {'et','rain','tide','diff','ks','slr'};
% 
% for j = [1,2,3,9]
%     ylimit = [-50 50];
%     figure('Color',[1 1 1],'Name',str2mat(vege_name(j)))
%     for i = 1 : 6
%         subplot(6,1,i)
%         plot(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         
%     end
%     ax.XTick = temp-731;
%     ax.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
%     
% end
% 
% for j = [6,7,8]
%     ylimit = [-15 15];
%     figure('Color',[1 1 1],'Name',str2mat(vege_name(j)))
%     for i = 1 : 6
%         subplot(6,1,i)
%         plot(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         
%     end
%     ax.XTick = temp-731;
%     ax.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
%     
% end
% 
% 
% %% plot all-structure - mean
% 
% % figure properties
% textsize = 14;
% textsizetitle = 16;
% 
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'S. virginica';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% % dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET','rain','river salinity','salt exchange','Ks','SLR'};
% strcname = {'et','rain','tide','diff','ks','slr'};
% 
% for j = [1,2,3,9]
%     ylimit = [-50 50];
%     figure('Color',[1 1 1],'Name',str2mat(vege_name(j)))
%     for i = 1 : 6
%         subplot(6,1,i)
%         plot(eval(str2mat(strcat('s(5).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         
%     end
%     ax.XTick = temp-731;
%     ax.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
%     
% end
% 
% for j = [6,7,8]
%     ylimit = [-15 15];
%     figure('Color',[1 1 1],'Name',str2mat(vege_name(j)))
%     for i = 1 : 6
%         subplot(6,1,i)
%         plot(eval(str2mat(strcat('s(5).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         
%     end
%     ax.XTick = temp-731;
%     ax.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
%     
% end

%% one param, selected classes, median
% 
% % figure properties
% textsize = 14;
% textsizetitle = 16;
% 
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'Sarcocornia sp.';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET','rain','river salinity','salt exchange','Ks','SLR'};
% strcname = {'et','rain','tide','diff','ks','slr'};
% 
% figure('Color',[1 1 1],'Name','median')
% for i = 1 : 6
%     subplot(7,1,i)
%     for j = [6,3]
%         %         ylimit = [-15 15];
%         n=7;
%         gcs = conv(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
%         plot(gcs,'.'); hold on
%         %         plot(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize,'XLim',[1 364],'TickLength',[0 0])
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         %         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         if i == 1
%             ax.YLim = [0 60];
%             legend(vege_name{6},vege_name{1},'Location','Northeast');
%         end
%     end
% end
% subplot(7,1,7)
% linewidth1 = 6;
% linewidth2 = 3;
% [hAx,hLine1,hLine2] = plotyy(17568:26304,S(17568:26304),17568:26304,rain(17568:26304)*100);
% ylabel(hAx(1),{'Duplin River';'Salinity'}) % left y-axis
% ylabel(hAx(2),'rain (mm day^-^1)','FontWeight','bold','FontSize',textsize) % right y-axis
% set(hAx,'FontSize',textsize,'FontWeight','Bold','XLim',[17568 26304],'TickLength',[0 0])
% hAx = gca;
% hAx.FontWeight = 'bold';
% hAx.FontSize = textsize;
% hAx.XTick = temp*24;
% hAx.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
% hAx.YGrid = 'on';
% % hAx.Position = [0.13 0.102990033222591 0.775 0.109597379364821];
% set(gca,'FontWeight','bold','FontSize',textsize)
% hLine1.LineWidth = linewidth1;
% hLine2.LineWidth = linewidth2;
% hLine1.Marker = '.';
% hAx.YTick = [10 20 30];
% ylim=[0 40];
% grid minor
% %%
% % smooth time series
% n = 1; % smoothing window (days)
% gcs = conv(gc(:,2), 1/n*ones(1,n), 'same');
% doct(:,2) = conv(doct(:,2), 1/n*ones(1,n), 'same');
% 
% %% one param, selected classes, mean
% 
% % figure properties
% textsize = 14;
% textsizetitle = 16;
% 
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/values_structure.mat')
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% 
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'Sarcocornia sp.';'shell';'open water';'upland'};
% 
% % first day of each month for 2014 and december 31,2014
% dayrecord = 732:1095;
% temp = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1095];
% params = {'ET','rain','river salinity','salt exchange','Ks','SLR'};
% strcname = {'et','rain','tide','diff','ks','slr'};
% 
% figure('Color',[1 1 1],'Name','median')
% for i = 1 : 6
%     subplot(7,1,i)
%     for j = [6,3]
%         %         ylimit = [-15 15];
%         n=7;
%         gcs = conv(eval(str2mat(strcat('s(5).',strcname(i),'(:,',num2str(j),')'))), 1/n*ones(1,n), 'same');
%         plot(gcs,'.'); hold on
%         %         plot(eval(str2mat(strcat('s(8).',strcname(i),'(:,',num2str(j),')'))),'.'); hold on
%         ylabel('% \Delta')
%         txt = char(str2mat(params(i)));
%         h = title(txt,'FontSize',textsizetitle);
%         set(h,'HorizontalAlignment','center');
%         set(gca,'FontWeight','bold','FontSize',textsize,'XLim',[1 364],'TickLength',[0 0])
%         ax = gca;
%         ax.XTick = zeros(1,0);
%         ax.XTickLabel = '';
%         ax.YGrid = 'on';
%         %         ax.YLim = ylimit;
%         set(gca,'FontWeight','bold','FontSize',textsize)
%         grid minor
%         if i == 1
%             ax.YLim = [0 60];
%             legend(vege_name{6},vege_name{2},'Location','Northeast');
%         end
%     end
% end
% subplot(7,1,7)
% linewidth1 = 6;
% linewidth2 = 3;
% [hAx,hLine1,hLine2] = plotyy(17568:26304,S(17568:26304),17568:26304,rain(17568:26304)*100);
% ylabel(hAx(1),{'Duplin River';'Salinity'}) % left y-axis
% ylabel(hAx(2),'rain (mm day^-^1)','FontWeight','bold','FontSize',textsize) % right y-axis
% set(hAx,'FontSize',textsize,'FontWeight','Bold','XLim',[17568 26304],'TickLength',[0 0])
% hAx = gca;
% hAx.FontWeight = 'bold';
% hAx.FontSize = textsize;
% hAx.XTick = temp*24;
% hAx.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};
% hAx.YGrid = 'on';
% % hAx.Position = [0.13 0.102990033222591 0.775 0.109597379364821];
% set(gca,'FontWeight','bold','FontSize',textsize)
% hLine1.LineWidth = linewidth1;
% hLine2.LineWidth = linewidth2;
% hLine1.Marker = '.';
% hAx.YTick = [10 20 30];
% ylim=[0 40];
% grid minor
% 
% 

%% one class, all parameters, 1st of each month
% load('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/sensitivityannual.mat')
% 
% 
% % first day of each month for 2014 and december 31,2014
% % dayrecord = [732;763;791;822;852;883;913;944;975;1005;1036;1066;1096];
% dayrecord = [732;763;791;822;852;883;913;944;975;1005;1036;1066] + 365;
% cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/baseline')
% for i = 1 : length(dayrecord)
%     filename = strcat('day',num2str(dayrecord(i)),'.mat');
%     load(filename,'S1')
%     eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
% end
% 
% % vegetation class names. position equals index value (e.g. TS = 8)
% vege_name = {'B. frutescens';'B. maritima';'J. roemerianus';'mud';'salt pan';...
%     'med. S. alterniflora';'short S. alterniflora';'tall S. alterniflora';...
%     'S. virginica';'shell';'open water';'upland'};
% 
% veg = 6;
% I = find(vegetation_class_complete == veg);
% 
% VegIndex = (vegetation_class_complete == veg);
% 
% OutIndex = logical(ones(length(vegetation_class_complete),1));
% OutIndex(outofbounds,1) = logical(0);
% 
% GCE10Index(GCE10pointsNDM20,1) = logical(1);
% 
% Index = VegIndex .* OutIndex .* GCE10Index;
% Index = logical(Index);
% 
% % figure properties
% textsize = 14;
% textsizetitle = 16;
% ylimit = [-15 15];
% % ylimit = [-50 50]
% 
% 
% for i = 1 : length(dayrecord)
%     filename = strcat('day',num2str(dayrecord(i)),'.mat');
%     load(filename,'S1')
%     eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
% end
% for i = 1 : 12 % months
%     Sallbase(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%     [x,~,C] = find (Sallbase(Index,i));
%     J{i} = x;
%     Sbase{i} = C;
%     clear eval x C
% end
% 
% figure('Color',[1 1 1],'name',cell2mat(vege_name(veg)))
% 
% for k = 1 : 6
%     
%     if k == 1 % ET 10% increase
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/et')
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     elseif k == 2 % rain 10% increase
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/precipitation')
%         
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     elseif k == 3 % diffusion 100% increase
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/saltexchange')
%         
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     elseif k == 4 % Ks order of mag increase
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/ks')
%         
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     elseif k == 5 % 5cm SLR
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/5cmSLR')
%         
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     elseif k == 6 % salinity 10% increase
%         cd('/Users/david/Dropbox/school/research/soil model/model paper/sims/sensitivity/output/tidesalinity')
%         
%         for i = 1 : length(dayrecord)
%             filename = strcat('day',num2str(dayrecord(i)),'.mat');
%             load(filename,'S1')
%             eval(strcat('Sday',num2str(dayrecord(i)),' = S1;'))
%         end
%         
%         for i = 1 : 12 % months
%             SallP(:,i)= eval(strcat('Sday',num2str(dayrecord(i)),'(1,:)'))';
%             temp(:,i) = 100*(SallP(:,i)-Sallbase(:,i))./Sallbase(:,i);
%             [x,~,C] = find (temp(Index,i));
%             Pmean{i} = C;
%             clear eval x C
%         end
%         
%     end
%     
%     
%     params = {'ET','rain','diff','Ks','SLR','tide salinity'};
%     %     figure('Color',[1 1 1])
%     subplot(6,1,k)
%     F = [Pmean{1}' Pmean{2}' Pmean{3}' Pmean{4}' Pmean{5}' Pmean{6}' ...
%         Pmean{7}' Pmean{8}' Pmean{9}' Pmean{10}' Pmean{11}' Pmean{12}'];
%     grpPet = [zeros(1,length(Pmean{1})),ones(1,length(Pmean{2})),ones(1,length(Pmean{3}))*3,ones(1,length(Pmean{4}))*4,...
%         ones(1,length(Pmean{5}))*5,ones(1,length(Pmean{6}))*6,ones(1,length(Pmean{7}))*7,ones(1,length(Pmean{8}))*8,...
%         ones(1,length(Pmean{9}))*9,ones(1,length(Pmean{10}))*10,ones(1,length(Pmean{11}))*11,ones(1,length(Pmean{12}))*12];
%     bh = boxplot(F,grpPet,...
%         'labels',{'','','','','','','','','','','',''},...
%         'Symbol',' ',...
%         'medianstyle','target',...
%         'boxstyle','outline');
%     a = char(vege_name(veg));
%     ylabel('% \Delta')
%     name = vege_name(veg);
%     %     txt =char([str2mat(name) ' ' str2mat(params(k))]);
%     txt = char(str2mat(params(k)));
%     h = title(txt,'FontSize',textsizetitle);
%     set(h,'HorizontalAlignment','center');
%     set(gca,'FontWeight','bold','FontSize',textsize)
%     for i=1:size(bh,1)
%         set(bh(i,:),'linewidth',2);
%     end
%     ax = gca;
%     ax.XTick = zeros(1,0);
%     ax.XTickLabel = '';
%     ax.YGrid = 'on';
%     %         ax.YTick = ytick;
%     ax.YLim = ylimit;
%     %         ax.Position = [0.13 0.788 0.775 0.117];
%     set(gca,'FontWeight','bold','FontSize',textsize)
%     grid minor
%     
%     clear P SallP temp
% end
% ax.XTick = 1:12;
% ax.XTickLabel = {'J','F','M','A','M','J','J','A','S','O','N','D'};