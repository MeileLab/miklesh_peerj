function [M1_h2o, M1_salt, M2_h2o, M2_salt]...
    = TIDAL( M1_h2o, M1_salt, M2_h2o, M2_salt, nu_now, nu_next, S, centroid_z, surface_area, rho_sw, dd, crit_flood_elements, porosity, ~, ~, ~)
% TIDAL: I?ll tell you why [religion is] not a scam, in my opinion. Tide
% goes in, tide goes out. Never a miscommunication. You can?t explain that.
% You can?t explain why the tide goes in... ~Bill O'Reilly

if nu_next > crit_flood_elements
    M2_fluid = ( nu_next - centroid_z ) * surface_area * rho_sw;
    M2_h2o = M2_fluid * ( 1 - S / 1000 );
    M2_salt = M2_fluid * S / 1000;
    if dd < centroid_z
        dM_fluid_tide = ( centroid_z - dd ) * porosity * surface_area * rho_sw;
        M1_h2o = M1_h2o + dM_fluid_tide * ( 1 - S / 1000 );
        M1_salt = M1_salt + dM_fluid_tide * S / 1000;
    end
elseif nu_now > crit_flood_elements
    if nu_next <= crit_flood_elements
        M2_fluid = ( crit_flood_elements - centroid_z ) * surface_area * rho_sw;
        M2_h2o = M2_fluid * ( 1 - S / 1000 );
        M2_salt = M2_fluid * S / 1000;
    end
end

end

