%% SOIL MODEL Miklesh_Meile_2018
% created by: David Miklesh
% date created: March 1, 2018
% base
%% INITIALIZATION

%
% MODEL CHARACTERISITCS
%

% Define location of load file
% load('decadalforcings.mat')
load('NDM20_Rs70_a10.mat')

% Load initial conditions
% 1 = LOAD IC FILE; 0 = NEW IC
IC = 0;
if IC == 0
    S0 = salinity(1); % initial porewater salinity
else
    load('day730.mat') % previous IC file
end

% initialhour = 2808;
initialhour = 1;
days = length(nu)/24; % length of simulation
% days = 1096;


%
% PEDON GEOMETRY AND CHARACTERISTICS
%
porosity = .45; %
Ks = 0.00072; %  low marsh saturated hydraulic conductivity (m/hr)
% k = 1.14E-13; %
viscosity = 1E-3;
depth = 0.3; % pedon depth (meter)
theta_s = porosity; % saturated water content
theta_r = 0.067; % residual water content 0.081
V0 = ( surface_area * depth * porosity )'; % porespace volume (cubic meter)
z_pedon = centroids(:,3) - depth; % Elevation of pedon bottom
rho_fw = 1000.8;
fc_mud = 0.966; % field capacity
fc_sand = 0.90;

%
% SALT EXCHANGE
%

% Diffusion coeffiecient
D = 7.25e-6; % meters^2/hour

% Characteristic length scale
dz = .01; % meters

% MISC. CONSTANTS
g = 9.8; % m/s


%
% SWITCHES AND SAVE PATH
%

% Switches

% Time series or average switch
% 1 = AVG ET AND PRECIPITATION; 0 = TIME SERIES INPUT
AVG_SWITCH = 0;

% Record change in salinity per process per time step
% 1 = RECORD dS PER PROCESS;
DS_SWITCH = 0;

% Output daily variables (once every 24 time steps)
% 1 = RECORD DAILY VARIABLES
RECORD_DAY = 1;

% Output hourly variables (every timestep)
% 1 = RECORD HOURLY VARIABLES
RECORD_HOUR = 0;

% Output the mass of salt and water after each process per time step
% 1 = RECORD MASS
RECORD_MASS = 0;

% Save path
pathname = '/lustre1/dmiklesh/4_year_outputs/sensitivity/baseline';


if AVG_SWITCH == 1
    S = 22; % tidal salinity (used when AVG_SWITCH = 1)
    rho_sw = rho_fw + 0.7925 * S; % tidal density (used when AVG_SWITCH = 1)
else
    rho_sw = rho_fw + 0.7925 .* salinity;
end

% display('SWITCHES AND PATH FINISHED!')

if IC == 1 % Start from previous point in time
    %     load( ICfilename );
    tt = days * 24 - 1; % hours
    zz = length( crit_flood_elements );
    
    %     load(ICfilename,'dd','S1','S2','V1','V2','M1_fluid','M1_h2o',...
    %         'M1_salt','M2_fluid','M2_h2o','M2_salt','rho1','rho2')
else % Start simulation with initial initialization
    %     load( ICfilename );
    tt = days * 24 - 1; % hours
    zz = length( crit_flood_elements );
    
    % INITIALIZE WATER LEVEL TO SEDIMENT MARSH PLATFORM
    %     dd(1,:) = centroids(:,3);
    
    % Porewater salinity initialzied based on elevation
    
    HHtide = max( nu ); % highest high tide
    %     S1 = zeros(1,zz);
    %     V1 = V0;
    %     theta = ones(1,zz) * theta_s;
    %     rho1 = zeros(1,zz);
    %     M1_fluid = zeros(1,zz);
    %     M1_h2o = zeros(1,zz);
    %     M1_salt = zeros(1,zz);
    S2 = zeros(1,zz);
    V2 = zeros(1,zz);
    rho2 = zeros(1,zz);
    M2_fluid = zeros(1,zz);
    M2_h2o = zeros(1,zz);
    M2_salt = zeros(1,zz);
    dd = zeros(1,zz);
    
    for ii = 1 : zz
        if centroids(ii,3) > 1.5
            S1(1,ii) = 0;
            rho1(1,ii) = rho_fw;
%                         M1_fluid(1,ii) = rho1( 1, ii) * V0(1,ii) ;
            
            M1_fluid(1,ii) = rho1( 1, ii) * V0(1,ii) * theta_r / porosity;
            M1_h2o(1,ii) = M1_fluid(1,ii) * ( 1 - S1(1,ii) / 1000 );
            M1_salt(1,ii) = M1_fluid(1,ii) * S1(1,ii) / 1000;
            dd(1,ii) = z_pedon(ii,1) + ( M1_h2o(1,ii)  + M1_salt(1,ii)  ) / rho1(1,ii) / surface_area(ii,1) / porosity;
            V1(1,ii) = ( M1_h2o(1,ii)+M1_salt(1,ii) ) / rho1(1,ii);
            theta(1,ii) = V1(1,ii) / ( V0(ii) / porosity );
        else
            S1( 1, ii) = S0;
            rho1(1,ii) = rho_fw(1) + 0.7925 * S1(1,ii);
            M1_fluid(1,ii) = rho1( 1, ii) * V0(1,ii);
            M1_h2o(1,ii) = M1_fluid(1,ii) * ( 1 - S1(1,ii) / 1000 );
            M1_salt(1,ii) = M1_fluid(1,ii) * S1(1,ii) / 1000;
            dd(1,ii) = centroids(ii,3);
            V1(1,ii) = V0(ii);
            theta(1,ii) =  theta_s;
        end
        %         M1_fluid(1,ii) = rho1( 1, ii) * V0(1,ii);
        %         M1_h2o(1,ii) = M1_fluid(1,ii) * ( 1 - S1(1,ii) / 1000 );
        %         M1_salt(1,ii) = M1_fluid(1,ii) * S1(1,ii) / 1000;
    end
end

% MATRIX INITIALIZATION
dM1_h2o = zeros(1,zz);
dM1_salt = zeros(1,zz);
dM2_h2o =  zeros(1,zz);
dM2_salt = zeros(1,zz);
dM_h2o_tide = zeros(1,zz);
dM_salt_tide = zeros(1,zz);
dM_h2o_ET = zeros(1,zz);
dM_h2o_rain = zeros(1,zz);
dM_h2o_drainage = zeros(1,zz);
dM_salt_drainage = zeros(1,zz);
dM_fluid_GW_multi = zeros(1,zz,3);
dM_h2o_GW_multi = zeros(1,zz,3);
dM_salt_GW_multi = zeros(1,zz,3);
dM_fluid_seepOUT_multi = zeros(1,zz,3);
dM_h2o_seepOUT_multi = zeros(1,zz,3);
dM_salt_seepOUT_multi = zeros(1,zz,3);
dM_fluid_seepIN_multi = zeros(1,zz,3);
dM_h2o_seepIN_multi = zeros(1,zz,3);
dM_salt_seepIN_multi = zeros(1,zz,3);
dM_h2o_GW = zeros(1,zz);
dM_fluid_GW = zeros(1,zz);
dM_salt_GW = zeros(1,zz);
dM_h2o_total = zeros(1,zz);
dM_salt_total = zeros(1,zz);
dM_fluid_1to2 = zeros(1,zz);
dM_h2o_1to2 = zeros(1,zz);
dM_salt_1to2 = zeros(1,zz);
dM_fluid_drainage = zeros(1,zz);
dM_fluid_tide = zeros(1,zz);
dM_h2o_2to1 = zeros(1,zz);
dM_salt_2to1 = zeros(1,zz);
dM_h2o_star = zeros(1,zz);
dM_fluid_total = zeros(1,zz);
dM1_fluid = zeros(1,zz);
dM2_fluid = zeros(1,zz);
dM_salt_star = zeros(1,zz);
dM_fluid_surface_multi = zeros(1,zz,3);
dM_h2o_surface_multi = zeros(1,zz,3);
dM_salt_surface_multi = zeros(1,zz,3);
dH_surface_multi = zeros(1,zz,3);
V1to2 = zeros(1,zz);
V2star = zeros(1,zz);
S_dM = zeros(1,zz);
S1_b4 = zeros(1,zz);
del_Pcreek = zeros(1,zz);
vcreek = zeros(1,zz);
dM_fluid_creekOUT = zeros(1,zz);
dM_h2o_creekOUT = zeros(1,zz);
dM_salt_creekOUT = zeros(1,zz);

P = zeros(1,zz);
del_P = zeros(zz,3);
del_Pseep = zeros(zz,3);
v = zeros(zz,3);
vseep = zeros(zz,3);
% theta = zeros(1,zz);
K = zeros(1,zz);
dVGW = zeros(zz,3);
dVseep = zeros(zz,3);
beta = zeros(1,zz);

ddstar = zeros(1,zz);
ddtide = zeros(1,zz);
tidal_flooding = zeros(1,zz);
distribute_mass = zeros(1,zz);
dist_mass_star = zeros(1,zz);

% transfer of mass indices for subsurface
parfor ii = 1 : zz
    for j = 1 : 3
        if neighbors_variable(ii,j) ==-1
            transloc{ii,j} = [-1, -1];
        else
            % array of neighbors for element receiving mass
            neighbor_take = neighbors_variable( neighbors_variable(ii,j), : );
            
            % determine column of element from which mass flowed
            [junk,col_give] = ind2sub([1, 3 ],find( neighbor_take == ii ));
            
            transloc{ii,j} = [neighbors_variable(ii,j), col_give];
        end
    end
end

disp('INITIALIZATION DONE!')

%%
% EXCHANGES
exchange_time = datestr(now, 'd:HH:MM:SS');
display( exchange_time );
disp( 'START WAITING!' );
%S
% TIME LOOP (hours)
%
for t = initialhour : tt
%     disp( 'source/sink' );
%     display( t );
%     
        if mod(t,24)==0
            display(t/24)
        end
    %
    % EXCHANGES AT ONE LOCATION
    %
    
    % ELEMENTS LOOP
%     tic
    parfor i = 1 : zz
        
        %
        % TIDAL FLOODING
        %
        if DS_SWITCH == 1
            S1_b4(1,i) = S1(1,i);
        end
        % Call TIDAL. Compute mass change due to tidal ebb and flow.
        if AVG_SWITCH == 1
            [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i)]...
                = TIDAL( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), nu( t ), nu( t+1 ), salinity, centroids(i,3), surface_area(i),...
                rho_sw, dd(1,i), crit_flood_elements(i), porosity, Ks, depth, theta_r );
            
            % Compute salinity, density, and volume.
            % Box 1
            [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
            % Box 2c
            [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
            
            % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
            if  M2_h2o(1,i) > eps
                if V1(1,i) < V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                end
            end
            
            % Sediment is "OVERSATURATED".
            if V1(1,i) > V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
            end
            
            % Calculate WATERLEVEL (dd) based on mass and density of water.
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
            
        else
            
            [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i)]...
                = TIDAL( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), nu( t ), nu( t+1 ), salinity(t), centroids(i,3), surface_area(i),...
                rho_sw(t), dd(1,i), crit_flood_elements(i), porosity, Ks, depth, theta_r );
            
            % Compute salinity, density, and volume.
            % Box 1
            [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
            % Box 2
            [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
            
            % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
            if  M2_h2o(1,i) > eps
                if V1(1,i) < V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                end
            end
            
            % Sediment is "OVERSATURATED".
            if V1(1,i) > V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
            end
            
            % Calculate WATERLEVEL (dd) based on mass and density of water.
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
            
        end
        if RECORD_MASS == 1
            M1h20tide(1,i) = M1_h2o(1,i);
            M1salttide(1,i) = M1_salt(1,i);
            M2h20tide(1,i) = M2_h2o(1,i);
            M2salttide(1,i) = M2_salt(1,i);
            V2tide(1,i) = V2(1,i);
            S2tide(1,i) = S2(1,i);
            S1tide(1,i) = S1(1,i);
        end
        
        %
        % EVAPOTRANSPIRATION
        %
        if DS_SWITCH == 1
            dS1_tidal(1,i) = S1(1,i) - S1_b4(1,i);
            S1_b4(1,i) = S1(1,i);
        end
        
        theta(1,i) = V1(1,i) / ( V0(i) / porosity ); % theta <= residual water content ET shuts off
        
        if theta(1,i) > (theta_r*1.5)
            el_class = vegetation_class_complete(i);
            if AVG_SWITCH == 1
                % Variable 'ET_class_complete' contains the daily average ET rate.
                % Variable 'vegetation_class_complete contains the associated
                % vegetation class for an elemenet.
                
                % Call EVAPOTRANSPIRATION_AVG. Computes mass change due to average ET and
                % removes mass depending on water level.
                [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), dd(1,i)]...
                    = EVAPOTRANSPIRATION( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), dd(1,i), centroids(i,3), surface_area(i), rho_fw, ET_class_rates( el_class )/24, porosity, Ks, depth );
                
                % Compute salinity, density, and volume.
                % Box 1
                [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
                % Box 2
                [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
                
                % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
                if  M2_h2o(1,i) > eps
                    if V1(1,i) < V0(i)
                        [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                            M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                            = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                    end
                end
                
                % Sediment is "OVERSATURATED".
                if V1(1,i) > V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
                end
                
                % Calculate WATERLEVEL (dd) based on mass and density of water.
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                    = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                    V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                    V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
                
            else
                % Call EVAPOTRANSPIRATION. Computes mass change due to ET and
                % removes mass depending on water level.
                [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), dd(1,i)]...
                    = EVAPOTRANSPIRATION( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), dd(1,i), centroids(i,3), surface_area(i), rho_fw, EThour(el_class,t), porosity, Ks, depth );
                
                
                % Compute salinity, density, and volume.
                % Box 1
                [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
                % Box 2
                [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
                
                % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
                if  M2_h2o(1,i) > eps
                    if V1(1,i) < V0(i)
                        [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                            M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                            = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                    end
                end
                
                % Sediment is "OVERSATURATED".
                if V1(1,i) > V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
                end
                
                % Calculate WATERLEVEL (dd) based on mass and density of water.
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                    = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                    V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                    V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
            end
        end
        %         end
        
        if RECORD_MASS == 1
            M1h20ET(1,i) = M1_h2o(1,i);
            M1saltET(1,i) = M1_salt(1,i);
            M2h20ET(1,i) = M2_h2o(1,i);
            M2saltET(1,i) = M2_salt(1,i);
            V2ET(1,i) = V2(1,i);
            S2ET(1,i) = S2(1,i);
            S1ET(1,i) = S1(1,i);
        end

        %
        % PRECIPITATION
        %
        if DS_SWITCH == 1
            dS1_ET(1,i) = S1(1,i) - S1_b4(1,i);
            S1_b4(1,i) = S1(1,i);
        end
        
        if AVG_SWITCH == 1
            %         S1_b4(1,i) = S1(1,i);
            % Call PRECIPITATION. Compute mass change due to precipitation and
            % adds mass to appropriate box depending on WATERLEVEL.
            [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i)]...
                = PRECIPITATION( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i),...
                M2_salt(1,i), centroids(i,3), surface_area(i), rho_fw, rain_avg/24, dd(1,i), porosity, Ks, depth, theta_r );
            
            % Compute salinity, density, and volume.
            % Box 1
            [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
            % Box 2
            [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
            
            % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
            if  M2_h2o(1,i) > eps
                if V1(1,i) < V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                end
            end
            
            % Sediment is "OVERSATURATED".
            if V1(1,i) > V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
            end
            
            % Calculate WATERLEVEL (dd) based on mass and density of water.
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
            
        else
            % Call PRECIPITATION. Compute mass change due to precipitation and
            % adds mass to appropriate box depending on WATERLEVEL.
            [M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i)]...
                = PRECIPITATION( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i),...
                M2_salt(1,i), centroids(i,3), surface_area(i), rho_fw, rain( t ), dd(1,i), porosity, Ks, depth, theta_r );
            
            % Compute salinity, density, and volume.
            % Box 1
            [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
            % Box 2
            [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
            
            % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
            if  M2_h2o(1,i) > eps
                if V1(1,i) < V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                end
            end
            
            % Sediment is "OVERSATURATED".
            if V1(1,i) > V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
            end
            
            % Calculate WATERLEVEL (dd) based on mass and density of water.
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
        end
        
        if RECORD_MASS == 1
            M1h20rain(1,i) = M1_h2o(1,i);
            M1saltrain(1,i) = M1_salt(1,i);
            M2h20rain(1,i) = M2_h2o(1,i);
            M2saltrain(1,i) = M2_salt(1,i);
            V2rain(1,i) = V2(1,i);
            S2rain(1,i) = S2(1,i);
            S1rain(1,i) = S1(1,i);
        end

        %
        % SALT EXCHANGE
        %
        if DS_SWITCH == 1
            dS1_rain(1,i) = S1(1,i) - S1_b4(1,i);
            S1_b4(1,i) = S1(1,i);
        end
        
        if  V2 (1,i) > 0
            
            % Call DIFFUSION. Calculate diffusion if sediment is flooded.
            [dM_salt_diff] = SALTEXCHANGE( M1_salt(1,i), V1(1,i), M2_salt(1,i), V2(1,i), surface_area(i), porosity, Ks, depth, D, dz );
            
            M1_salt (1,i) = M1_salt (1,i) + dM_salt_diff;
            M2_salt (1,i) = M2_salt (1,i) - dM_salt_diff;
            
            % Compute salinity, density, and volume.
            % Box 1
            [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
            % Box 2
            [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
            
            % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
            if  M2_h2o(1,i) > eps
                if V1(1,i) < V0(i)
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                        = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                end
            end
            
            % Sediment is "OVERSATURATED".
            if V1(1,i) >   V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
            end
            
            % Calculate WATERLEVEL (dd) based on mass and density of water.
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
        end
        
        if RECORD_MASS == 1
            M1h20diff(1,i) = M1_h2o(1,i);
            M1saltdiff(1,i) = M1_salt(1,i);
            M2h20diff(1,i) = M2_h2o(1,i);
            M2saltdiff(1,i) = M2_salt(1,i);
            V2diff(1,i) = V2(1,i);
            S2diff(1,i) = S2(1,i);
            S1diff(1,i) = S1(1,i);
        end

        %
        % DRAINAGE
        %
        if DS_SWITCH == 1
            dS1_diff(1,i) = S1(1,i) - S1_b4(1,i);
            S1_b4(1,i) = S1(1,i);
        end
        if isnan(M1_h2o(1,i)) == 1
            i
            error('drain b4')
        end
        if nu(t) < centroids(i,3)
            if isnan(VertDrainDist( i,1 )) == 0
                
                % water content
                theta(1,i) = V1(1,i) / ( V0(i) / porosity );
                
                %                 if theta(1,i) > theta_r
                veg = vegetation_class_complete(i);
                if veg == 1 || veg == 2 || veg == 3 || veg == 9 || veg == 12
                    fc = fc_sand;
                else
                    fc = fc_mud;
                end
                
                if theta(1,i) > fc*theta_s % field capacity
                    % Call DRAINAGE. Calculate diffusion if sediment is flooded.
                    % VertDrainDist(i,1) = discharge node; VertDrainDist(i,2) =
                    % horizontal distance
                    
                    % head difference
                    dh = dd(1,i) - nu(t);
                    [dM_h2o_drainage, dM_salt_drainage,drain] = DRAINAGE( rho1(1,i), S1(1,i), surface_area(i), porosity, Ks*10, dh, VertDrainDist(i,2));
                    vdrain(i) = drain;
                    M1_h2o (1,i) = M1_h2o (1,i) + dM_h2o_drainage;
                    M1_salt (1,i) = M1_salt (1,i) + dM_salt_drainage;
                    
                    if isnan(M1_h2o(1,i)) == 1
                        i
                        error('drain after comp')
                    end
                    
                    % Compute salinity, density, and volume.
                    % Box 1
                    [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
                    % Box 2
                    [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
                    
                    if isnan(M1_h2o(1,i)) == 1
                        i
                        error('drain after rhovs')
                    end
                    
                    % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
                    if  M2_h2o(1,i) > eps
                        if V1(1,i) < V0(i)
                            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                                = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
                        end
                    end
                    if isnan(M1_h2o(1,i)) == 1
                        i
                        error('drain after redist')
                    end
                    
                    % Sediment is "OVERSATURATED".
                    if V1(1,i) >   V0(i)
                        [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                            M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                            = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
                    end
                    if isnan(M1_h2o(1,i)) == 1
                        i
                        error('drain after over')
                    end
                    
                    % Calculate WATERLEVEL (dd) based on mass and density of water.
                    [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                        M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
                        = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
                        V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
                        V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
                    
                    if isnan(M1_h2o(1,i)) == 1
                        i
                        error('drain after dd')
                    end
                    
                end
            end
        end
        if DS_SWITCH == 1
            dS1_drain(1,i) = S1(1,i) - S1_b4(1,i);
        end
        
        if RECORD_MASS == 1
            M1h20drain(1,i) = M1_h2o(1,i);
            M1saltdrain(1,i) = M1_salt(1,i);
            M2h20drain(1,i) = M2_h2o(1,i);
            M2saltdrain(1,i) = M2_salt(1,i);
            V2drain(1,i) = V2(1,i);
            S2drain(1,i) = S2(1,i);
            S1drain(1,i) = S1(1,i);
        end
        
        % Pressure head (Pascals). Used for del_P computation in subsurface
        % flow.
        if dd(1,i) <= centroids(i,3)
            P(1,i) = (dd(1,i)+20) * rho1(1,i) * g;
            Pseep(1,i) = (dd(1,i) - z_pedon(i)) * rho1(1,i) * g;
        else
            P(1,i) = (centroids(i,3)+20) * rho1(1,i) * g...
                + ( dd(1,i) - centroids(i,3) ) * rho2(1,i) * g;
            Pseep(1,i) = 0;
        end
        
        
    end
    % toc

    %
    %   EXCHANGES WITH NEIGHBORS
    %
    
%     disp( 'subsurface' );
%     display( t );
    
    dVGW = zeros(zz,3);
    dVseep = zeros(zz,3);
    dM_h2o_GW_multi= zeros(zz,3);
    dM_salt_GW_multi= zeros(zz,3);
    dM_h2o_seepIN_multi= zeros(zz,3);
    dM_salt_seepIN_multi= zeros(zz,3);
    dM_h2o_seepOUT_multi= zeros(zz,3);
    dM_salt_seepOUT_multi= zeros(zz,3);
    
    % loop over elements
    % tic
    for i = 1 : zz
        
        %
        % SUBSURFACE
        %
        
        % water content
        theta(1,i) = V1(1,i) / ( V0(i) / porosity );
        
        if (theta(1,i)-theta_s)  > eps
            display(i);
            error('GW: theta > theta_s')
        end
        
        % fitting parameters
        
        Kss = Ks;
        veg = vegetation_class_complete(i);
        if veg == 1 || veg == 2|| veg == 3|| veg == 9|| veg == 12
            Kss = Ks*10;
        else
            
        end
        a = 1; %(m^-1)
        Hp = dd(1,i) - centroids(i,3);
        K(1,i) = Kss*exp(a*min(0,Hp)); % Gardner 1958
        if K(1,i) < 0
            error('Se is negative!');
        end
        
        % compute mass flux
        for yy = 1 : 3
            if ( neighbors_variable (i,yy) == -1 )
                del_P(i,yy) = 0;
                del_Pseep(i,yy) = 0;
                v(i,yy) = 0;
                vseep(i,yy) = 0;
            elseif theta(1,i) > theta_r % water will not flow at residual water content
                del_P(i,yy) = ( P(1,neighbors_variable(i,yy) ) - P(1,i) ) / dx(i,yy);
                if (nu(t) < z_pedon(i))
                    if (nu(t) < centroids(transloc{i,yy}(1),3))
                        del_Pseep(i,yy) = - Pseep(1,i) / dx(i,yy)/2;
                    else
                        del_Pseep(i,yy) = 0;
                    end
                else
                    del_Pseep(i,yy) = 0;
                end
                % compute Darcy velocities (m/s)
                v(i,yy) = - ( K(i) / (g * rho1(1,i))) * del_P(i,yy) * cosd(alpha(i,yy));
                vseep(i,yy) = - (K(i) / (g * rho1(1,i))) * del_Pseep(i,yy) * cosd(alpha(i,yy));
            else
                del_P(i,yy) = 0;
                del_Pseep(i,yy) = 0;
                v(i,yy) = 0;
                vseep(i,yy) = 0;
            end
            
            
            
            if v(i,yy) > 0
                if theta(1,i) > theta_r
                    if nu(t) <= centroids(neighbors_variable(i,yy),3)
                        
                        % GW subtract mass flowing out
                        %                         dM_fluid_GW_multi(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * rho1(1,i) * porosity;
                        dM_h2o_GW_multi(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * rho1(1,i) * porosity * ( 1 - S1(1,i) / 1000 );
                        dM_salt_GW_multi(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * rho1(1,i) * porosity * S1(1,i) / 1000;
                        dVGW(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * porosity;
                        
                        
                        % SEEPAGE subtract mass flowing out
                        %                         dM_fluid_seepOUT_multi(i,yy) = - vseep(i,yy) * area_neighbor_above(i,yy) * rho1(1,i) * porosity;
                        dM_h2o_seepOUT_multi(i,yy) = - vseep(i,yy) * area_neighbor_above(i,yy) * rho1(1,i) * porosity * ( 1 - S1(1,i) / 1000 );
                        dM_salt_seepOUT_multi(i,yy) = - vseep(i,yy) * area_neighbor_above(i,yy) * rho1(1,i) * porosity * S1(1,i) / 1000;
                        dVseep(i,yy) = - vseep(i,yy) * area_neighbor_below(i,yy) * porosity;
                        
                    else
                        % GW subtract mass flowing out
                        dM_h2o_GW_multi(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * rho1(1,i) * porosity * ( 1 - S1(1,i) / 1000 );
                        dM_salt_GW_multi(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * rho1(1,i) * porosity * S1(1,i) / 1000;
                        dVGW(i,yy) = - v(i,yy) * area_neighbor_below(i,yy) * porosity;
                        
                        % SEEPAGE subtract mass flowing out
                        dM_h2o_seepOUT_multi(i,yy) = 0;
                        dM_salt_seepOUT_multi(i,yy) = 0;
                        dVseep(i,yy) = 0;
                        
                    end
                end
            end
        end
        
    end
    % toc
    
    for i = 1 : zz
        % check if mass goes residual
        if round((V1(1,i) + dVGW(i,1) + dVGW(i,2) + dVGW(i,3) + dVseep(i,1) + dVseep(i,2) + dVseep(i,3)),3) < round((V0(1,i)*theta_r),3)
%             beta = (V1(1,i) - V0(1,i)*theta_r) / (abs(sum(dVGW(i,:)))+abs(sum(dVseep(i,:))))
%             display(V0(1,i)*theta_r)
%             display(V1(1,i) + dVGW(i,1) + dVGW(i,2) + dVGW(i,3) + dVseep(i,1) + dVseep(i,2) + dVseep(i,3))
%             display((V1(1,i) + dVGW(i,1) + dVGW(i,2) + dVGW(i,3) + dVseep(i,1) + dVseep(i,2) + dVseep(i,3))/(V0(1,i)*theta_r))
%             error('low volume')
            beta(i) = (V1(1,i) - V0(1,i)*theta_r) / (abs(sum(dVGW(i,:)))+abs(sum(dVseep(i,:)))); % compute fraction so residual saturation is not reached
            
            % adjust mass flux
            dM_h2o_GW_multi(i,:) = dM_h2o_GW_multi(i,:) * beta(i);
            dM_salt_GW_multi(i,:) = dM_salt_GW_multi(i,:) * beta(i);
            dM_h2o_seepOUT_multi(i,:) = dM_h2o_seepOUT_multi(i,:) * beta(i);
            dM_salt_seepOUT_multi(i,:) = dM_salt_seepOUT_multi(i,:) * beta(i);
            
            if isnan(dM_h2o_GW_multi(i,:))
                display(i)
                disp('GW nan')
            end
        end
        
        % neighbor 1
        if transloc{i,1}(1) ~= -1
            dM_h2o_GW_multi(transloc{i,1}(1),transloc{i,1}(2)) = -dM_h2o_GW_multi(i,1);
            dM_salt_GW_multi(transloc{i,1}(1),transloc{i,1}(2)) = -dM_salt_GW_multi(i,1);
            dM_h2o_seepIN_multi(transloc{i,1}(1),transloc{i,1}(2)) = -dM_h2o_seepOUT_multi(i,1);
            dM_salt_seepIN_multi(transloc{i,1}(1),transloc{i,1}(2)) = -dM_salt_seepOUT_multi(i,1);
        end
        
        % neighbor 2
        if transloc{i,2}(1) ~= -1
            dM_h2o_GW_multi(transloc{i,2}(1),transloc{i,2}(2)) = -dM_h2o_GW_multi(i,2);
            dM_salt_GW_multi(transloc{i,2}(1),transloc{i,2}(2)) = -dM_salt_GW_multi(i,2);
            dM_h2o_seepIN_multi(transloc{i,2}(1),transloc{i,2}(2)) = -dM_h2o_seepOUT_multi(i,2);
            dM_salt_seepIN_multi(transloc{i,2}(1),transloc{i,2}(2)) = -dM_salt_seepOUT_multi(i,2);
        end
        
        % neighbor 3
        if transloc{i,3}(1) ~= -1
            dM_h2o_GW_multi(transloc{i,3}(1),transloc{i,3}(2)) = -dM_h2o_GW_multi(i,3);
            dM_salt_GW_multi(transloc{i,3}(1),transloc{i,3}(2)) = -dM_salt_GW_multi(i,3);
            dM_h2o_seepIN_multi(transloc{i,3}(1),transloc{i,3}(2)) = -dM_h2o_seepOUT_multi(i,3);
            dM_salt_seepIN_multi(transloc{i,3}(1),transloc{i,3}(2)) = -dM_salt_seepOUT_multi(i,3);
        end
    end
    
    % Sum mass exchange with neighbors.
    % tic
    parfor i = 1 : zz
        if DS_SWITCH == 1
            S1_b4(1,i) = S1(1,i);
        end
        % GW
        dM_h2o_GW(1,i) = sum(dM_h2o_GW_multi(i,:));
        dM_salt_GW(1,i) = sum(dM_salt_GW_multi(i,:));
        %         dM_h2o_GW(1,i) = dM_h2o_GW_multi(i,1)...
        %             + dM_h2o_GW_multi(i,2)...
        %             + dM_h2o_GW_multi(i,3);
        %         dM_salt_GW(1,i) = dM_salt_GW_multi(i,1)...
        %             + dM_salt_GW_multi(i,2)...
        %             + dM_salt_GW_multi(i,3);
        
        % SEEPAGE
        dM_h2o_seepOUT(1,i) = sum(dM_h2o_seepOUT_multi(i,:));
        dM_salt_seepOUT(1,i) = sum(dM_salt_seepOUT_multi(i,:));
        %         dM_h2o_seepOUT(1,i) = dM_h2o_seepOUT_multi(i,1)...
        %             + dM_h2o_seepOUT_multi(i,2)...
        %             + dM_h2o_seepOUT_multi(i,3);
        %         dM_salt_seepOUT(1,i) = dM_salt_seepOUT_multi(i,1)...
        %             + dM_salt_seepOUT_multi(i,2)...
        %             + dM_salt_seepOUT_multi(i,3);
        
        dM_h2o_seepIN(1,i) = sum(dM_h2o_seepIN_multi(i,:));
        dM_salt_seepIN(1,i) = sum(dM_salt_seepIN_multi(i,:));
        %         dM_h2o_seepIN(1,i) = dM_h2o_seepIN_multi(i,1)...
        %             + dM_h2o_seepIN_multi(i,2)...
        %             + dM_h2o_seepIN_multi(i,3);
        %         dM_salt_seepIN(1,i) = dM_salt_seepIN_multi(i,1)...
        %             + dM_salt_seepIN_multi(i,2)...
        %             + dM_salt_seepIN_multi(i,3);
        
        
        M1_h2o(1,i) = M1_h2o(1,i) + dM_h2o_GW(1,i) + dM_h2o_seepOUT(1,i);
        M1_salt(1,i) = M1_salt(1,i) + dM_salt_GW(1,i) + dM_salt_seepOUT(1,i);
        M2_h2o(1,i) = M2_h2o(1,i) + dM_h2o_seepIN(1,i);
        M2_salt(1,i) = M2_salt(1,i) + dM_salt_seepIN(1,i);
        
        % Compute salinity, density, and volume.
        % Box 1
        [S1(1,i), rho1(1,i), V1(1,i)] = S_RHO_V( M1_salt(1,i), M1_h2o(1,i) );
        % Box 2
        [S2(1,i), rho2(1,i), V2(1,i)] = S_RHO_V( M2_salt(1,i), M2_h2o(1,i) );
        
        % REDISTRIBUTE mass if soil is unsaturated and mass in box 2.
        if  M2_h2o(1,i) > eps
            if V1(1,i) < V0(i)
                [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                    M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                    = REDISTRIBUTE( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), V2(1,i), rho2(1,i), S2(1,i) );
            end
        end
        
        % Sediment is "OVERSATURATED".
        if V1(1,i) > V0(i)
            [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
                M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i)]...
                = OVERSATURATED( M1_h2o(1,i), M1_salt(1,i), M2_h2o(1,i), M2_salt(1,i), V1(1,i), V0(i), rho1(1,i), S1(1,i) );
        end
        
        % Calculate WATERLEVEL (dd) based on mass and density of water.
        [M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i), V1(1,i), M2_h2o(1,i),...
            M2_salt(1,i), S2(1,i), rho2(1,i), V2(1,i), dd(1,i)]...
            = WATERLEVEL( M1_h2o(1,i), M1_salt(1,i), S1(1,i), rho1(1,i),...
            V1(1,i), M2_h2o(1,i), M2_salt(1,i), S2(1,i), rho2(1,i),...
            V2(1,i), surface_area(i), centroids(i,3), z_pedon(i), porosity, V0(i), theta_r, rho_fw);
        
        % water content
        theta(1,i) = V1(1,i) / ( V0(i) / porosity );
        
        if DS_SWITCH ==1
            dS1_GW(1,i) = S1(1,i) - S1_b4(1,i);
        end
        
        if RECORD_MASS == 1
            M1h20GW(1,i) = M1_h2o(1,i);
            M1saltGW(1,i) = M1_salt(1,i);
            M2h20GW(1,i) = M2_h2o(1,i);
            M2saltGW(1,i) = M2_salt(1,i);
            V2GW(1,i) = V2(1,i);
            S2GW(1,i) = S2(1,i);
            S1GW(1,i) = S1(1,i);
        end
        
    end
    
    % toc
    
    
    % SAVE VARIABLES
    if RECORD_DAY == 1
        % Save variables to .mat files at specified intervals
        if mod(t,24)==0
            dayrecord = t/24;
            filename = strcat('day',num2str(dayrecord),'.mat');
            f = fullfile(pathname,filename);
            save(f,'dd','S1','theta')
            
        end
    end
    
    if DS_SWITCH == 1
        if mod(t,1)==0
            
            filename = strcat('dS',num2str(t),'.mat');
            f = fullfile(pathname,filename);
            save(f,'dS1_ET','dS1_rain','dS1_tidal',...
                'dS1_drain','dS1_diff','dS1_GW')
        end
    end
    
    if RECORD_HOUR == 1
        if mod(t,1)==0
            
            filename = strcat('hour',num2str(t),'.mat');
            f = fullfile(pathname,filename);
            save(f,'dd','S1','S2','V1','V2','rho1','rho2','theta')
        end
    end
    
    if RECORD_MASS == 1
        if mod(t,1)==0
            
            filename = strcat('mass',num2str(t),'.mat');
            f = fullfile(pathname,filename);
            save(f,'M1h20ET','M1saltET','M2h20ET','M2saltET','V2ET','S2ET','S1ET',...
                'M1h20rain','M1saltrain','M2h20rain','M2saltrain','V2rain','S2rain','S1rain',...
                'M1h20tide','M1salttide','M2h20tide','M2salttide','V2tide','S2tide','S1tide',...
                'M1h20diff','M1saltdiff','M2h20diff','M2saltdiff','V2diff','S2diff','S1diff',...
                'M1h20drain','M1saltdrain','M2h20drain','M2saltdrain','V2drain','S2drain','S1drain',...
                'M1h20GW','M1saltGW','M2h20GW','M2saltGW','V2GW','S2GW','S1GW')
            
        end
    end
    
end

end_time = datestr(now, 'd:HH:MM:SS');
f = fullfile(pathname,'endofsim.mat');
display( exchange_time );
display( end_time );

disp( 'DONE' )
