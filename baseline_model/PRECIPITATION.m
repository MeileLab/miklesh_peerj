function [M1_h2o, M1_salt, M2_h2o, M2_salt] = PRECIPITATION( M1_h2o, M1_salt, M2_h2o, M2_salt, centroid_z, surface_area, rho_fw, rain, dd, porosity, Ks, depth, theta_r )
% PRECIPITATION: Compute mass change due to precipitation and adds mass
% to appropriate box depending on waterlevel.

%     'above'
if dd  >= centroid_z
    dHrain  = rain;

%     'below'
else
    dHrain = rain / porosity; 
end
% Update water level (estimate)

dd_rain  = dd  + dHrain;

dM_h2o_rain  = rain * surface_area * rho_fw( 1 );


% Update box mass based on water level
% 'mass rain'

% % Box 2 (rising)
if ( dd  >= centroid_z )
    M2_h2o  = M2_h2o  + dM_h2o_rain;

% Both boxes (rising)
elseif ( ( dd  < centroid_z ) && ( dd_rain  >=  centroid_z ) )
    phi = ( ( centroid_z - dd  ) * ( porosity ) ) / ( ( centroid_z - dd_rain  )...
        * ( porosity ) + ( dd_rain  - centroid_z ) )  ;
    M1_h2o  =  M1_h2o  + phi * dM_h2o_rain; 
    M2_h2o  = ( 1 - phi ) * dM_h2o_rain;
    
% Box 1 (rising)
elseif  ( dd_rain  < ( centroid_z - eps ) )
    M1_h2o  = M1_h2o  + dM_h2o_rain;



end

% if dd >= centroid_z
%     M2_h2o  = M2_h2o  + dM_h2o_rain;
% else
%     M1_h2o  = M1_h2o  + dM_h2o_rain;
% end

end

