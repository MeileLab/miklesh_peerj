function [dM_salt_diff] = SALTEXCHANGE( M1_salt, V1, M2_salt, V2, surface_area, porosity, Ks, depth, D, dz)
% DIFFUSION. Salt exchange between surface and subsurface. 

% % diffusion coeffiecient 
% % D = 7.25e-6; % meters^2/hour
% 
% % characteristic length
% dz = .05; % meters


dM_salt_diff = - D * ( ( M1_salt / V1 ) - ( M2_salt / V2 ) ) / dz * surface_area * porosity; 

end

