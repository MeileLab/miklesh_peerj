function [dM_h2o_drainage, dM_salt_drainage,v] = DRAINAGE( rho, S, surface_area, porosity, Ks, dh, dx)
% DRAINAGE. Mass is removed from box 1. Mass from box 2 will fill box 1.

v = Ks * dh / dx;

dM_fluid_drainage  = - rho * v * surface_area * porosity;
dM_h2o_drainage  = dM_fluid_drainage  * ( 1 - S / 1000 );
dM_salt_drainage  = dM_fluid_drainage  * S / 1000;

end
