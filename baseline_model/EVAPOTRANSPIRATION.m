function [M1_h2o, M1_salt, M2_h2o, M2_salt, dd] = EVAPOTRANSPIRATION( M1_h2o, M1_salt, M2_h2o, M2_salt, dd, centroid_z, surface_area, rho_fw, ET_hour, porosity, ~, ~ )
% EVAPOTRANSPIRATION: Compute mass change due to ET.

dM_h2o_ET  = - ET_hour * surface_area * rho_fw;

% Update water level (estimate)
ddET  = dd - ET_hour;

% Update box mass based on water level

% Box 2 (falling)
if ddET >= centroid_z
    M2_h2o  = M2_h2o  + dM_h2o_ET;
    
    % Both boxes (falling)
elseif dd  > centroid_z
    if ddET  <  centroid_z
        blu = ( ( centroid_z - ddET  ) * ( porosity ) ) / ( ( centroid_z - ddET  )...
            * ( porosity ) + ( dd  - centroid_z ) );
        M1_h2o  =  M1_h2o  + blu * dM_h2o_ET;
        M1_salt = M1_salt + M2_salt;
        M2_h2o  = 0;
        M2_salt = 0;
    end
    
    % Box 1 (falling)
else
    M1_h2o  = M1_h2o  + dM_h2o_ET;
end

% if M1_h2o < 0
%     M1_h2o = 0;
% end

end

