function [M1_h2o, M1_salt, S_1, rho1, V1, M2_h2o, M2_salt, S_2, rho2, V2, dd ]...
    = WATERLEVEL( M1_h2o, M1_salt, S_1, rho1, V1, M2_h2o, M2_salt, S_2, rho2, V2, surface_area, centroid_z, z_pedon, porosity, V0, theta_r, rho_fw)
% WATERLEVEL: Compute the water level.

% Water level above ground.
if M2_h2o > eps
    dd = centroid_z + ( M2_h2o + M2_salt ) / rho2 / surface_area;
    
    % Water level in pedon
elseif (( M2_h2o <= eps ) && ( M1_h2o  > eps ))
    %     if ( M1_h2o  > eps )
    M2_h2o = 0;
    M2_salt = 0;
    S_2  = 0;
    rho2  = 0;
    V2  = 0;
    dd  = z_pedon + ( M1_h2o  + M1_salt  ) / rho1 / surface_area / porosity;
    %     end
    % Water level below pedon. Set water level to pedon.
else
    S_1 = 0;
    rho1 = rho_fw;
    M1_fluid= rho1 * V0 * theta_r;
    M1_h2o = M1_fluid * ( 1 - S_1 / 1000 );
    M1_salt = M1_fluid* S_1 / 1000;
    dd = z_pedon + ( M1_h2o  + M1_salt  ) / rho1 / surface_area / porosity;
    V1 = ( M1_h2o + M1_salt ) / rho1;
    %     error('negative mass in waterlevel')
end

