function [S, rho, V ] = S_RHO_V( mass_salt, mass_h2o )
% S_rho_V: Compute salinity, density, and volume

% if  mass_h2o > eps
if  mass_h2o > 0
    mass_salt = max ( 0, mass_salt );
    S = mass_salt * 1000 / ( mass_h2o + mass_salt );
    rho = 1000.821 + 0.7925 * S;
    V = ( mass_h2o + mass_salt ) / rho;
else
    S = 0;
    rho = 0;
    V = 0;
end

end

