function [M1_h2o, M1_salt, S1, rho1, V1, M2_h2o, M2_salt, S2, rho2, V2 ]...
    = REDISTRIBUTE( M1_h2o, M1_salt, M2_h2o, M2_salt, V1, V0, V2, rho2, S2 )
% REDISTRIBUTE: Redistribute mass if soil is unsaturated and mass in box 2.
% Quick fix

V2to1 = V0 - V1;
if V2to1 >= V2
    % Move all above ground mass to below ground
    M1_h2o = M1_h2o + M2_h2o;
    M1_salt = M1_salt + M2_salt;
    M2_h2o  = 0;
    M2_salt  = 0;
else
    M1_h2o = M1_h2o + (V2to1 * rho2) * ( 1 - S2 / 1000 );
    M1_salt = M1_salt + (V2to1 * rho2) * S2 / 1000;
    M2_h2o  = M2_h2o - (V2to1 * rho2) * ( 1 - S2 / 1000 );
    M2_salt  = M2_salt - (V2to1 * rho2) * S2 / 1000;
end

% Compute salinity, density, and volume.
[S1, rho1, V1] = S_RHO_V( M1_salt , M1_h2o);
[S2, rho2, V2] = S_RHO_V( M2_salt , M2_h2o);

% Check if sediment is "oversaturated".
if V1 > V0 
    [M1_h2o, M1_salt, S1, rho1, V1, M2_h2o, M2_salt, S2, rho2, V2]...
        = OVERSATURATED( M1_h2o, M1_salt, M2_h2o, M2_salt, V1, V0, rho1, S1 );
end

end

