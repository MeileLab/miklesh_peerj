#PBS -S /bin/bash
#PBS -q batch
#PBS -N sa_baseline
#PBS -l nodes=1:ppn=28:Intel
#PBS -l walltime=60:00:00
#PBS -l mem=16gb
#PBS -M dmiklesh@uga.edu 
#PBS -m abe

cd $PBS_O_WORKDIR

module load matlab/R2017b

echo
echo "Job ID: $PBS_JOBID"
echo "Queue:  $PBS_QUEUE"
echo "Cores:  $PBS_NP"
echo "Nodes:  $(cat $PBS_NODEFILE | sort -u | tr '\n' ' ')"
echo

matlab -nodisplay < SALTMODELv30.m > matlab_${PBS_JOBID}.out
