function [M1_h2o, M1_salt, S1, rho1, V1, M2_h2o, M2_salt, S2, rho2, V2]...
    = OVERSATURATED( M1_h2o, M1_salt, M2_h2o, M2_salt, V1, V0, rho1, S1 )
% OVERSATURATED: If the volume of the porewater is greater that the
% porespace, then mass is redistributed above ground.

% Move excess mass back above ground.
V1to2 = V1 - V0;
V1 = V0;
dM_fluid_1to2 = V1to2 * rho1;
dM_h2o_1to2 = dM_fluid_1to2 * ( 1 - S1 / 1000 );
dM_salt_1to2 = dM_fluid_1to2 * S1 / 1000;
M1_h2o = M1_h2o - dM_h2o_1to2;
M1_salt = M1_salt - dM_salt_1to2;
M2_h2o = M2_h2o + dM_h2o_1to2;
M2_salt = M2_salt + dM_salt_1to2;

[S2, rho2, V2] = S_RHO_V( M2_salt, M2_h2o );

end

