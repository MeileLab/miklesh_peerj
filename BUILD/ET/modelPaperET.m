%% TIME SERIES ET [day] for model paper
% Input
%   * Meteorological data (MET-GCEM)
%   * alpha, the albedo
%   * h, crop height
% Output
%   * ET [m/day]
%   * ET_hourly [m/hr]

load('marshlanding_daily_jan03-dec16_vars.mat')
load('VegCharacteristics.mat','VegHeight_Albedo')
% load('year_concat_1035088.mat','daily_global_horizontal_radiation')
load('

% observed radiation (MJ/m^2/d)
% Rs = Daily_Total_Total_Solar_Rad * 10^-3; % [kJ] to [MJ]
Rs = daily_global_horizontal_radiation(:,2)/1e6*3600; % W/m2/day to MJ/m2/day

% temperature
Tmin = Daily_Min_Mean_Temperature_Air(1:4748); % minimum daily temperature [�C]
Tmax = Daily_Max_Mean_Temperature_Air(1:4748); % maximum daily temperature [�C]
Tmean = Daily_Mean_Mean_Temperature_Air(1:4748); % mean daily temperature[�C]

% relative humidity
RHmean = Daily_Mean_Humidity(1:4748);

% atmospheric pressure [kPa]
P = Daily_Mean_Baro_Pressure(1:4748) / 10; % [mbar]-->[kPa]

% wind speed [m/s]
u10 = Daily_Mean_Mean_Wind_Speed(1:4748);

n = length(Rs); % number of days to be computed from dataset

% Assign variable names (may be redundant)
% Mean_Temp_Air = Mean_Temperature_Air;
% Max_Temp_Air = Max_Temperature_Air;
% Min_Temp_Air = Min_Temperature_Air;
% Total_Solar_Rad = Total_Solar_Rad;
% Humidity = Humidity;
% Baro_Press = Baro_Pressure;
% Wind_Speed = Mean_Wind_Speed;
% 

% Set period
% n = length( Mean_Temp_Air ) /4/24; % number of days to be computed from dataset
% nsum = 96; % number of data points in one day 15-min = 96
% 
% for i = 1 : n
%     % define indices
%     n1 = 1 + nsum * (i-1);
%     n2 = nsum * i;
%     
%     % observed radiation (MJ/m^2/d)
%     Rs(i) = sum( Total_Solar_Rad(n1:n2) ) * 10^-3; % [kJ] to [MJ]
%     
%     % temperature
%     Tmin(i) = min( Min_Temp_Air(n1:n2) ); % minimum daily temperature [�C]
%     Tmax(i) = max( Max_Temp_Air(n1:n2) ); % maximum daily temperature [�C]
%     Tmean(i) = sum( Mean_Temp_Air(n1:n2) ) / nsum; % mean daily temperature[�C]
%     
%     % relative humidity
%     RHmean(i) = sum( Humidity(n1:n2) ) / nsum;
%     
%     % atmospheric pressure [kPa]
%     P(i) = sum( Baro_Press(n1:n2) ) / nsum / 10; % [mbar]-->[kPa]
%     
%     % wind speed [m/s]
%     u10(i) = sum( Wind_Speed(n1:n2) ) / nsum;
%     
% end


for m = 1 : 12
    m
    h = VegHeight_Albedo(m,1);
    alpha = VegHeight_Albedo(m,2);
    
    %     % observed radiation (MJ/m^2/d)
    %     Rs = Daily_Total_Total_Solar_Rad * 10^-3; % [kJ] to [MJ]
    %
    %     % temperature
    %     Tmin = Daily_Min_Temp_Air; % minimum daily temperature [�C]
    %     Tmax = Daily_Max_Temp_Air; % maximum daily temperature [�C]
    %     Tmean = Daily_Mean_Temp_Air; % mean daily temperature[�C]
    %
    %     % relative humidity
    %     RHmean = Daily_Mean_Humidity;
    %
    % atmospheric pressure [kPa]
    %     P = Daily_Mean_Baro_Press / 10; % [mbar]-->[kPa]
    
    % wind speed [m/s]
    %     u10 = Daily_Mean_Wind_Speed / nsum;
    %     u10 = 3.7495 * ones(3159,1);
    
    % RADIATION [MJ/m^2/d]
    % Extraterresstrial radiation (Ra)
    J = 1 : n; % creates a vector of days
    Gsc = 0.0820; % solar constant [MJ/m^2/min]
    phi = 0.548344; % latitude [rad]
    dr = 1 + 0.033 * cos( 2 * pi .* J / 365 ); % inverse relative distance Earth-Sun, J is day number between 1-365
    delta = 0.409 * sin( ( 2 * pi .* J / 365 ) - 1.39); % solar declination [rad]
    ws = acos( -tan(phi) .* tan(delta) ); %sunset hour angle
    Ra = ( 24 * 60 * Gsc .* dr / pi .* ( ws .* sin(phi) .* sin(delta) + cos(phi) .* cos(delta) .* sin(ws) ) );
    
    % Clear-sky radiation (Rso)
    as = 0.25; % recommended Allen et al. (1998)
    bs = 0.50; % recommended Allen et al. (1998)
    Rso = (as + bs) .* Ra';
    
    % Net shortwave radiation (Rns)
    Rns = (1 - alpha) .* Rs;
    
    % Net longwave radiaton (Rnl)
    sigma = 4.903 * 10^-9; % Stefan-Boltzman constant (MJ/K^4/m^2/d)
    eomax = 0.6108 * exp( 17.27 .* Tmax ./ ( Tmax + 237.3 ) ); % saturation vapor pressure at air temp Tmax (kPa)
    eomin = 0.6108 * exp( 17.27 .* Tmin ./ ( Tmin + 237.3 ) ); % saturation vapor pressure at air temp Tmin (kPa)
    es = (eomax + eomin) ./ 2; % mean saturation vapor pressure (kPa)
    ea = RHmean .* es / 100; % actual vapor pressure
    TminK = Tmin + 273.15; % [K]
    TmaxK = Tmax + 273.15; % [K]
    Rnl = sigma .* ( (TminK.^4 +TmaxK.^4) / 2 ) .* ( 0.34 - 0.14 .* ea.^.5 ) .* ( 1.35 .* ( Rs ./ Rso ) - 0.35 );
    
    % Net radiation (Rn)
    Rn = Rns - Rnl;
    
    % Soil heat flux (G)
    G = 0; % for periods 1-day or longer G=0;
    
    % OTHER COMPUTATIONS AND VALUES
    del = 2504 * exp( (17.27 * Tmean) ./ (Tmean + 237.3) ) ./ (Tmean + 237.3).^2; % slope of saturation vapor pressure curve [kPa/�C]
    lambda = 2.45; % latent heat of vaporization [MJ/kg]
    % zp = 2; % height of pressure [m]
    % P = 101.3 * ( ( 293 - 0.0065 * zp ) / 293 )^5.26;
    psychro = 0.665 * 10^-3 * P; % psychrometric constant [kPa/�C]
    zw = 10; % heigh of wind measurement [m]
    u2 = u10 .* ( 4.87 / log( 67.8 * zw - 5.42 ) ); % wind speed at 2m [m/s]
    P0 = 101.3; % atm. pressure for coastal areas (Wang et al., 2007) [kPa]
    Tkv = 1.01 .* ( Tmean + 273.15 ); % virtual air temperautre (Wang et al., 2007) [K]
    R = 0.287; % specific gas constant [kJ/kg/K]
    cp = 1.013 * 10^-3; % specific heat of air at constant pressure [MJ/kg/�C]
    rhomeanair = P0 ./ Tkv ./ R; % mean air density at constant pressure [kg/m^3]
    rhowater = 1000.821; % freshwater density [kg/m^3]
    
    zm = 2; % height of wind measurement [m]
    zh = 2; % height of humidity measurement [m]
    d = (2/3) * h; % zero plane displacement height [M]
    zom = 0.123 * h; % roughness length governing momentum transfer [m]
    zoh = 0.1 * zom; % roughness length governing transfer of heat and vapor[m]
    k = 0.41; % von Karman's constant [-]
    ra = (log((zm - d) / zom) * log((zh - d) / zoh)) ./ ( k^2 * u2 );
%     ranum = sum(~isnan(ra));
%     rara = ra(isnan(ra) == 0);
%     ramean(m,1) = sum(rara)/ranum;
    
    % ra = 2; % Hughes et al. (2001), Wang et al. (2007)
    
    if h == 0.002
        rs = 0; % open water
    else
        %         ri = 100; % stomatal resistance of a single leaf under well-watered conditions [s/m]
        %         % LAI = 24 * h; % Allen et al. (1998)
        %         LAI = 1.5 * log(h) + 5.5;
        %         % LAI = 7.3; % As reported in B.L. Howes et al.: tall (1m) S.Alterniflora (7.3), short (0.3m) S.Alterniflora (1.9)
        %         LAIactive = 0.5 * LAI; % active (sunlit) leaf area index [m^2(leaf area) m^-2(soil surface)]
        %         rs = ri / LAIactive % (Bulk) surface resistance [s/m]
        rs = 70; % Souch et al. (1998), Wang et al. (2007)
    end
    
    % Latent heat flux [MJ/m^2/d]
    latentET = ( del .* (Rn - G) + rhomeanair .* cp .* (es - ea) ./ ra ) ./ ( del + psychro .* ( 1 + rs ./ ra) );
    ETday(m,:) = (latentET ./ lambda ./ rhowater); % daily ET [m/day]
    
    %     Remove NaN and replace with ETday rate from previous day
    NaNday = find( isnan( ETday(m,:) ) );
    for i = 1 : length( NaNday )
        if i == 1
            ETday( m, NaNday( i ) ) = 0;
        else
            ETday( m, NaNday( i ) ) = ETday( m, NaNday( i ) - 1);
        end
    end
    
    EThourtemp = [];
    
    for d = 1 : n
%         EThourtemp = [];
        for t = 1 : 24
            if (t > 6 && t <= 18)
                ETtemp(1,t) = - (( t^2 - 24*t +144 ) * ETday(m,d)) / 288 + 0.125 * ETday(m,d);
            else
                ETtemp(1,t) = 0;
            end
        end
        EThourtemp = horzcat(EThourtemp, ETtemp);
    end
    
    EThour(m,:) = EThourtemp;
        
end

%% concat radiation data and create global horizontal irradiance time series
% columns of .mat: year, month, day, hour, minute, DHI (w/m2), DNI (w/m2),
% GHI (w/m2)

% load .mat file contain individual years
load('year_concat_1035088.mat')

% concat years 2003 - 2015
allyears = vertcat(x2003,x2004,x2005,x2006,x2007,x2008,x2009,x2010,x2011,x2012,x2013,x2014,x2015);

% compute serial day number
serialday = datenum(allyears(:,1),allyears(:,2),allyears(:,3));

% compute daily total global horizontal irradiane (W/m2)

for i = 1 : length(serialday)/24
    i1 = 24*i-23;
    i2 = 24*i;
    
    daily_global_horizontal_radiation(i,1) = floor(serialday(i1)); % serial day number
    daily_global_horizontal_radiation(i,2) = nansum(allyears(i1:i2,8)); % global horizontal irradiance (W/m2)
    
end

%%

figure
plot(daily_global_horizontal_radiation(:,1),daily_global_horizontal_radiation(:,2)/1e6*3600,'.'); hold on
plot(daily_global_horizontal_radiation(:,1),Rso,'.r')
datetick('x',10)
ylabel('MJ m^{-2} day^{-1}')
legend('global horizontal irradiance','clear-sky radiation')

figure
plot(daily_global_horizontal_radiation(:,1),ETday70(6,:),'.')
datetick('x',10)

%%

figure
plot(serial_date_day,radiation_global_horizontal_total,'.')
datetick('x',10)

figure
plot(daily_global_horizontal_radiation(:,1),daily_global_horizontal_radiation(:,2),'.'); hold on