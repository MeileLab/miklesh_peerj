% evaporation

load('marshlanding_daily_jan14-dec14_vars.mat')
load('VegCharacteristics.mat','VegHeight_Albedo')
load('ETtimeseries.mat')

% Assign variable names (may be redundant)
% Mean_Temp_Air = Mean_Temp_Air;
% Max_Temp_Air = Max_Temp_Air;
% Min_Temp_Air = Min_Temp_Air;
% Total_Solar_Rad = Total_Solar_Rad;
% Humidity = Humidity;
% Baro_Press = Baro_Press;
% Wind_Speed = Wind_Speed;

Mean_Temp_Air = Daily_Mean_Temp_Air;
Max_Temp_Air = Daily_Max_Temp_Air;
Min_Temp_Air = Daily_Min_Temp_Air;
Total_Solar_Rad = Daily_Total_Total_Solar_Rad;
Humidity = Daily_Mean_Humidity;
Baro_Press = Daily_Mean_Baro_Press;
Wind_Speed = Daily_Mean_Wind_Speed;

alpha = 0.05;

% Set period
nsum = 1; % number of data points in one day 15-min = 96
n = length( Mean_Temp_Air ) /nsum; % number of days to be computed from dataset


for i = 1 : n
    % define indices
    n1 = 1 + nsum * (i-1);
    n2 = nsum * i;
    
    % observed radiation (MJ/m^2/d)
    Rs(i) = sum( Total_Solar_Rad(n1:n2) ) * 10^-3; % [kJ] to [MJ]
    
    % temperature
    Tmin(i) = min( Min_Temp_Air(n1:n2) ); % minimum daily temperature [�C]
    Tmax(i) = max( Max_Temp_Air(n1:n2) ); % maximum daily temperature [�C]
    Tmean(i) = sum( Mean_Temp_Air(n1:n2) ) / nsum; % mean daily temperature[�C]
    
    % relative humidity
    RHmean(i) = sum( Humidity(n1:n2) ) / nsum;
    
    % atmospheric pressure [kPa]
    P(i) = sum( Baro_Press(n1:n2) ) / nsum / 10; % [mbar]-->[kPa]
    
    % wind speed [m/s]
    u10(i) = sum( Wind_Speed(n1:n2) ) / nsum;
    
end

C = 0.599; % S = 35 solute concentration (mol/L)
rhow = 1000; % density of liquid water (kg/m3)
theta = 0.45; % water content
thetas = 0.45; % saturated water content
thetar = 0.067; % residual water content
dprime = 1.8; % Hoff dissociation factor for NaCl
Se = (theta - thetar)/(thetas - thetar); % effective satuartion
n = 1.41; % fitting parameter
m = 1-1/n; % fitting parameter
a = 1;


Tw = 20; % water temp (�C)
TwK = Tw + 273.16; % water temp (K)
M = 0.0180; % molecular weight of water (kg/mol)
R = 8.314; % universal gas constant ( J/mol/K)
g = 9.8; % acceleration due to gravity (m/s2)


ha = RHmean/100; % relative humidity of air at air temp

u = u10 *86.4; % average wind speed (km/day)
psychro = 0.665 * 10^-3 * P * 7.50062/10; % psychrometric constant [cmHg/�C]

psim = (1/a)*(Se^(1/m) - 1)^(1/n); % matric potential (m)
psio = dprime*(R*Tw*C)/(g*rhow); % osmotic potential (m)

hm = exp(M*g*psim/(R*Tw)); % humidity matric potential
ho = exp(M*g*psio/(R*Tw)); % humidity osmotic potential
hs = hm * ho; % relative humidity of soil

eomax = 0.6108 * exp( 17.27 .* Tmax ./ ( Tmax + 237.3 ) ); % saturation vapor pressure at air temp Tmax (kPa)
eomin = 0.6108 * exp( 17.27 .* Tmin ./ ( Tmin + 237.3 ) ); % saturation vapor pressure at air temp Tmin (kPa)
esata = (eomax + eomin) ./ 2 * 7.50062/10; % mean saturation vapor pressure of air (cmHg) 
esats = 0.6108 * exp( 17.27 .* Tw ./ ( Tw + 237.3 ) ) * 7.50062/10; % saturation vapor pressure of soil (cmHg) 
ea = RHmean .* esata / 100; % actual vapor pressure


% RADIATION [MJ/m^2/d]
% Extraterresstrial radiation (Ra)
J = 1 : n; % creates a vector of days
Gsc = 0.0820; % solar constant [MJ/m^2/min]
phi = 0.548344; % latitude [rad]
dr = 1 + 0.033 * cos( 2 * pi .* J / 365 ); % inverse relative distance Earth-Sun, J is day number between 1-365
delta = 0.409 * sin( ( 2 * pi .* J / 365 ) - 1.39); % solar declination [rad]
ws = acos( -tan(phi) .* tan(delta) ); %sunset hour angle
Ra = ( 24 * 60 * Gsc .* dr / pi .* ( ws .* sin(phi) .* sin(delta) + cos(phi) .* cos(delta) .* sin(ws) ) );

% Clear-sky radiation (Rso)
as = 0.25; % recommended Allen et al. (1998)
bs = 0.50; % recommended Allen et al. (1998)
Rso = (as + bs) .* Ra;
Rns = (1 - alpha) .* Rs;

% Net longwave radiaton (Rnl)
sigma = 4.903 * 10^-9; % Stefan-Boltzman constant (MJ/K^4/m^2/d)

TminK = Tmin + 273.15; % [K]
TmaxK = Tmax + 273.15; % [K]
Rnl = sigma .* ( (TminK.^4 +TmaxK.^4) / 2 ) .* ( 0.34 - 0.14 .* ea.^.5 ) .* ( 1.35 .* ( Rs ./ Rso ) - 0.35 );


% Net radiation (Rn)
Rn = Rns - Rnl; % (MJ/m2/day)
H = Rn / 2.45 / 10 ; % evaporation equivalent (cm/day)




% Eas = 0.35 .* (hs .* esats - ha .* esata) .* ( 0.5 + u./161); % drying power of the atmoshpere
Eas = 0.35 .* (hs .* esata*2 - ha .* esata) .* ( 0.5 + u./161); % drying power of the atmoshpere

delta = 7.50062/10 * 2504 * exp( (17.27 * Tmean) ./ (Tmean + 237.3) ) ./ (Tmean + 237.3).^2; % slope of saturation vapor pressure curve [cmHG/�C]

Es = (hs .* delta .* H + psychro .* Eas) ./ (hs .* delta +psychro); % (cm/day)

figure
plot(Es(1:365),'.'); hold on
ylabel('cm day^{-1}')
plot(ETday2014(4,:)*100,'pr')
legend('evaporation','ET')


% derp = Es(1,1:365)'\(ETday2014(4,:)*100)';
derp = (ETday2014(4,:)*100)'\Es(1,1:365)';
title(['gamma = ' num2str(derp) ' albedo = ' num2str(alpha)])

% derp = nanmean(Es(1,1:365)./ETday2014(4,:)',2);
% title(num2str(derp))