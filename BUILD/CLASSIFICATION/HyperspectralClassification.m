%% Assign mesh prefix
prefix = 'NDM20_unsmoothed';
load(strcat(prefix,'_mesh','.mat'))
%% Read in ESRI hyperspectral data
% The function m_shaperead is the m_map toolbox.

% M=m_shaperead('DupClass_MLC');
load('HyperspectralPolygons.mat')
%% Plot polygons
clf;
for k=1:length(M.ncst)
    
    line(M.ncst{k}(:,1),M.ncst{k}(:,2)); % creates outline of region
    
end

%% Hyperspectral Assignment
% Determining which polygon a mesh-element centroids falls with in, a
% vegetation class will be defined for that element.

start_time = datestr(now, 'd:HH:MM:SS');

% Input mesh
% elements_variable = NDM15_elements;
% nodes_variable = NDM15_nodes;
% neighbors_variable = NDM15_neighbors;

% Initialize vector
vegetation_class = zeros( length( centroids), 1 );


jj = length( M.ncst );
xcent = centroids( :, 1 );
ycent = centroids( :, 2 );
polygons = M.ncst;
classes = M.CLASS_ID;
parfor i = 1 : length( centroids )
    i;
    
    for j = 1 : jj
        
        %         poly = M.ncst{ j };
        poly = polygons{ j };
        xv = poly( :, 1 );
        yv = poly( :, 2 );
        
        % Determine if centroids is withing polygon
        %         in = inpolygon( centroids( i, 1 ), centroids( i, 2 ), xv, yv ); % true of false
        in = inpolygon( xcent(i), ycent(i), xv, yv ); % true of false
        
        
        if in == 1
            %             vegetation_class( i ) = M.CLASS_ID{ j };
            vegetation_class( i ) = classes{ j };
            break % break out of for loop after finding polygon
        end
    end
end

start_time
end_time = datestr(now, 'd:HH:MM:SS')

% save incase of error later on. 
save(strcat(prefix,'_polygon_assign','.mat'),'vegetation_class')

%% Plot Vegetation Map: Hyperspectral assignment
figure
hold on
% colorVec = hsv(max(vegetation_class));
% colorVec = parula(max(vegetation_class));
colorVec = jet(max(vegetation_class));

a = NaN * ones(12);

for i = 1 : 10
    plot(a(i),'.','Color',colorVec(i,:),'markersize',50); % dummy plot (NaN's do not appear)
end
legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
    'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
    'S.virginica','Oyster bed','Open water','Upland',...
    'Location','eastoutside')
set(gca,'FontWeight','bold','FontSize',18)

for i = 1 : max(vegetation_class)
    clear I
    I = find(vegetation_class == i );
    
    plot3(centroids(I,1),centroids(I,2),centroids(I,3),'.','Color',colorVec(i,:),'Markersize',1); hold on
end

az = 0;
el = 90;
view(az, el);
axis square
axis equal
title('MODEL DOMAIN: VEGETATION CLASS MAP w/ UNASSIGNED ELEMENTS')
set(gca,'FontWeight','bold','FontSize',16)

% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])
%% Define vegetation class for unassigned mesh elements
% Ater running 'Define vegetation class for mesh elements' some elements
% remain assigned because element centroidss did not fall within vegetation
% class polygons. Using mean elevation cover class data from Hladik's
% dissertation unassigned elements will be assigned tall, medium, or short
% S. alterniflora, of J. roemerianus based on element centroids elevation.


% mean vegetation class elevation and standard deviation (Hladik and
% Alaber, 2012)
TSmean = 0.563;
MSmean = 0.973;
SSmean = 1.073;
MUDmean = 1.093;
SVmean = 1.153;
BMmean = 1.193;
SALTmean = 1.213;
JRmean = 1.223;
BFmean = 1.433;

TSSD = 0.27;
MSSD = 0.13;
SSSD = 0.1;
MUDSD = 0.1;
SVSD = 0.07;
BMSD = 0.07;
SALTSD = 0.06;
JRSD = 0.2;
BFSD = 0.1;

% vegetation class elevation ranges (meters). Mid points between the mean
% elevations of a vegetation class
low_T_spartina = 0.293;
low_M_spartina = 0.768;
low_S_spartina = 1.023;
low_mud = 1.083;
low_S_virginica = 1.123;
low_B_maritima = 1.153;
low_salpan = 1.203;
low_J_roemerianus = 1.218;
low_B_frutescens = 1.328;
high_B_frutescens = 1.533;


% determine unassigned elements
[ unassigned_elements ] = find( vegetation_class == 0 );

vegetation_class_complete = vegetation_class;

% define upland elements
uplandelev = BFmean + BFSD; % elements above B. frutescens range are classified as upland
[indUPLAND] = find( centroids(unassigned_elements,3) >= uplandelev );
vegetation_class_complete( unassigned_elements( indUPLAND ) ) = 12;

% define open water elements

% All unassigned elements are labled as 0, which SHOULD be all elements
% with a centroids elevation less than low_T_spartina that is assigned
% above.

waterelev = TSmean - TSSD;
[indWATER] = find( centroids(unassigned_elements,3) < waterelev );
vegetation_class_complete( unassigned_elements( indWATER ) ) = 11;

% find unassigned elements that are not open water or upland
[ unassigned_elements_after ] = find( vegetation_class_complete == 0 );

% classify unassigned elements based on elevation ranges

% tall spartina
IndTS = find((centroids(unassigned_elements_after,3)>=low_T_spartina) & (centroids(unassigned_elements_after,3)<low_M_spartina));
vegetation_class_complete(unassigned_elements_after(IndTS)) = 8;

% medium spartina
IndMS = find((centroids(unassigned_elements_after,3)>=low_M_spartina) & (centroids(unassigned_elements_after,3)<low_S_spartina));
vegetation_class_complete(unassigned_elements_after(IndMS)) = 6;

% short spartina
IndSS = find((centroids(unassigned_elements_after,3)>=low_S_spartina) & (centroids(unassigned_elements_after,3)<low_mud));
vegetation_class_complete(unassigned_elements_after(IndSS)) = 7;

% mud
Indmud = find((centroids(unassigned_elements_after,3)>=low_mud) & (centroids(unassigned_elements_after,3)<low_S_virginica));
vegetation_class_complete(unassigned_elements_after(Indmud)) = 4;

% Sarcocornia
IndSV = find((centroids(unassigned_elements_after,3)>=low_S_virginica) & (centroids(unassigned_elements_after,3)<low_B_maritima));
vegetation_class_complete(unassigned_elements_after(IndSV)) = 9;

% Batis maritima
IndBM = find((centroids(unassigned_elements_after,3)>=low_B_maritima) & (centroids(unassigned_elements_after,3)<low_salpan));
vegetation_class_complete(unassigned_elements_after(IndBM)) = 2;

% salt pan
Indsalt = find((centroids(unassigned_elements_after,3)>=low_salpan) & (centroids(unassigned_elements_after,3)<low_J_roemerianus));
vegetation_class_complete(unassigned_elements_after(Indsalt)) = 5;

% J. roemerianus
IndJR = find((centroids(unassigned_elements_after,3)>=low_J_roemerianus) & (centroids(unassigned_elements_after,3)<low_B_frutescens));
vegetation_class_complete(unassigned_elements_after(IndJR)) = 3;

% B. frutescens
IndBF = find((centroids(unassigned_elements_after,3)>=low_B_frutescens) & (centroids(unassigned_elements_after,3)<=high_B_frutescens));
vegetation_class_complete(unassigned_elements_after(IndBF)) = 1;

% open water
IndWater = find(centroids(unassigned_elements_after,3)<low_T_spartina);
vegetation_class_complete(unassigned_elements_after(IndWater)) = 11;

% verify that all elements are assigned a vegetation class. should be empty
[ unassigned_elements_after_assignment ] = find( vegetation_class_complete == 0 );

% save(strcat(prefix,'_vegetation_complete','.mat'),'vegetation_class_complete')

%% Fall within typical elevation range 
% Check that elements assigned using hyperspectral data have an elevation
% that falls one STD from the mean elevation for a given class
% Mean vegetation class elevation and standard deviation
TSmean = 0.563;
MSmean = 0.973;
SSmean = 1.073;
MUDmean = 1.093;
SVmean = 1.153;
BMmean = 1.193;
SALTmean = 1.213;
JRmean = 1.223;
BFmean = 1.433;

TSSD = 0.27;
MSSD = 0.13;
SSSD = 0.1;
MUDSD = 0.1;
SVSD = 0.07;
BMSD = 0.07;
SALTSD = 0.06;
JRSD = 0.2;
BFSD = 0.1;

% determine if an element's elevation is with the range for the
% classification. 

% 1 STD
z = 1;
% B. frutescens
for i = 1 : length(vegetation_class_complete)
    %     i
    if vegetation_class_complete(i) == 1
        if (centroids(i,3)>=(BFmean-BFSD))
            if (centroids(i,3)<=(BFmean+BFSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% B. maritima
    if vegetation_class_complete(i) == 2
        if (centroids(i,3)>=(BMmean-BMSD))
            if (centroids(i,3)<=(BMmean+BMSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% J. roemerianus    
    if vegetation_class_complete(i) == 3
        if (centroids(i,3)>=(JRmean-JRSD))
            if (centroids(i,3)<=(JRmean+JRSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% intertidal mud   
    if vegetation_class_complete(i) == 4
        if (centroids(i,3)>=(MUDmean-MUDSD))
            if (centroids(i,3)<=(MUDmean+MUDSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% salt pan    
    if vegetation_class_complete(i) == 5
        if (centroids(i,3)>=(SALTmean-SALTSD))
            if (centroids(i,3)<=(SALTmean+SALTSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
        
    end
% med. S. alterniflora    
    if vegetation_class_complete(i) == 6
        if (centroids(i,3)>=(MSmean-MSSD))
            if (centroids(i,3)<=(MSmean+MSSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
        
    end
% short S. alterniflora     
    if vegetation_class_complete(i) == 7
        if (centroids(i,3)>=(SSmean-SSSD))
            if (centroids(i,3)<=(SSmean+SSSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% tall S. alterniflora     
    if vegetation_class_complete(i) == 8
        if (centroids(i,3)>=(TSmean-TSSD))
            if (centroids(i,3)<=(TSmean+TSSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
% S. virginica    
    if vegetation_class_complete(i) == 9
        if (centroids(i,3)>=(SVmean-SVSD))
            if (centroids(i,3)<=(SVmean+SVSD))
                %break
            else
                outofbounds(z) = i;
                z = z + 1;
            end
        else
            outofbounds(z) = i;
            z = z + 1;
        end
    end
    
end

% 2 STD
z = 1;
% B. frutescens
for i = 1 : length(vegetation_class_complete)
    %     i
    if vegetation_class_complete(i) == 1
        if (centroids(i,3)>=(BFmean-BFSD*2))
            if (centroids(i,3)<=(BFmean+BFSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% B. maritima
    if vegetation_class_complete(i) == 2
        if (centroids(i,3)>=(BMmean-BMSD*2))
            if (centroids(i,3)<=(BMmean+BMSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% J. roemerianus    
    if vegetation_class_complete(i) == 3
        if (centroids(i,3)>=(JRmean-JRSD*2))
            if (centroids(i,3)<=(JRmean+JRSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% intertidal mud   
    if vegetation_class_complete(i) == 4
        if (centroids(i,3)>=(MUDmean-MUDSD*2))
            if (centroids(i,3)<=(MUDmean+MUDSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% salt pan    
    if vegetation_class_complete(i) == 5
        if (centroids(i,3)>=(SALTmean-SALTSD*2))
            if (centroids(i,3)<=(SALTmean+SALTSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
        
    end
% med. S. alterniflora    
    if vegetation_class_complete(i) == 6
        if (centroids(i,3)>=(MSmean-MSSD*2))
            if (centroids(i,3)<=(MSmean+MSSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
        
    end
% short S. alterniflora     
    if vegetation_class_complete(i) == 7
        if (centroids(i,3)>=(SSmean-SSSD*2))
            if (centroids(i,3)<=(SSmean+SSSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% tall S. alterniflora     
    if vegetation_class_complete(i) == 8
        if (centroids(i,3)>=(TSmean-TSSD*2))
            if (centroids(i,3)<=(TSmean+TSSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
% S. virginica    
    if vegetation_class_complete(i) == 9
        if (centroids(i,3)>=(SVmean-SVSD*2))
            if (centroids(i,3)<=(SVmean+SVSD*2))
                %break
            else
                outofbounds2std(z) = i;
                z = z + 1;
            end
        else
            outofbounds2std(z) = i;
            z = z + 1;
        end
    end
    
end

% 3 STD
z = 1;
% B. frutescens
for i = 1 : length(vegetation_class_complete)
    %     i
    if vegetation_class_complete(i) == 1
        if (centroids(i,3)>=(BFmean-BFSD*3))
            if (centroids(i,3)<=(BFmean+BFSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% B. maritima
    if vegetation_class_complete(i) == 2
        if (centroids(i,3)>=(BMmean-BMSD*3))
            if (centroids(i,3)<=(BMmean+BMSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% J. roemerianus    
    if vegetation_class_complete(i) == 3
        if (centroids(i,3)>=(JRmean-JRSD*3))
            if (centroids(i,3)<=(JRmean+JRSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% intertidal mud   
    if vegetation_class_complete(i) == 4
        if (centroids(i,3)>=(MUDmean-MUDSD*3))
            if (centroids(i,3)<=(MUDmean+MUDSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% salt pan    
    if vegetation_class_complete(i) == 5
        if (centroids(i,3)>=(SALTmean-SALTSD*3))
            if (centroids(i,3)<=(SALTmean+SALTSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
        
    end
% med. S. alterniflora    
    if vegetation_class_complete(i) == 6
        if (centroids(i,3)>=(MSmean-MSSD*3))
            if (centroids(i,3)<=(MSmean+MSSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
        
    end
% short S. alterniflora     
    if vegetation_class_complete(i) == 7
        if (centroids(i,3)>=(SSmean-SSSD*3))
            if (centroids(i,3)<=(SSmean+SSSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% tall S. alterniflora     
    if vegetation_class_complete(i) == 8
        if (centroids(i,3)>=(TSmean-TSSD*3))
            if (centroids(i,3)<=(TSmean+TSSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
% S. virginica    
    if vegetation_class_complete(i) == 9
        if (centroids(i,3)>=(SVmean-SVSD*3))
            if (centroids(i,3)<=(SVmean+SVSD*3))
                %break
            else
                outofbounds3std(z) = i;
                z = z + 1;
            end
        else
            outofbounds3std(z) = i;
            z = z + 1;
        end
    end
    
end

figure
plot(vegetation_class_complete(outofbounds),'o')
title('Out of range: 1 STD')
ylabel('class')
figure
plot(vegetation_class_complete(outofbounds2std),'o')
title('Out of range: 2 STD')
ylabel('class')
figure
plot(vegetation_class_complete(outofbounds3std),'o')
title('Out of range: 3 STD')
ylabel('class')

save(strcat(prefix,'_outofbounds','.mat'),'outofbounds','outofbounds2std','outofbounds3std')

% plot 1 std out of bounds
figure
hold on

% choose color scheme
% colorVec = hsv(max(vegetation_class_complete));
% colorVec = parula(max(vegetation_class_complete));
colorVec = jet(max(vegetation_class_complete));

a = NaN * ones(12);

for i = 1 : 12
    plot(a(i),'.','Color',colorVec(i,:),'markersize',50); % dummy plot (NaN's do not appear)
end
legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
    'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
    'S.virginica','Oyster bed','Open water','Upland',...
    'Location','eastoutside')
set(gca,'FontWeight','bold','FontSize',18)

for i = 1 : max(vegetation_class_complete)
    clear I
    I = find(vegetation_class_complete == i );
    
    plot3(centroids(I,1),centroids(I,2),centroids(I,3),'.','Color',colorVec(i,:),'Markersize',1); hold on
end

az = 0;
el = 90;
view(az, el);
axis square
axis equal
% title('MODEL DOMAIN: VEGETATION CLASS MAP w/ COMPLETE CLASSIFICATION')
title('out of bounds: 1 STD')
set(gca,'FontWeight','bold','FontSize',16)

% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])

% plot desired elements on top of map
plotpoints = outofbounds;
zzz = ones(length(plotpoints),1)*15;
plot3(centroids(plotpoints,1),centroids(plotpoints,2),zzz,'.k','Markersize',4);

% plot 2 std out of bounds
figure
hold on

% choose color scheme
% colorVec = hsv(max(vegetation_class_complete));
% colorVec = parula(max(vegetation_class_complete));
colorVec = jet(max(vegetation_class_complete));

a = NaN * ones(12);

for i = 1 : 12
    plot(a(i),'.','Color',colorVec(i,:),'markersize',50); % dummy plot (NaN's do not appear)
end
legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
    'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
    'S.virginica','Oyster bed','Open water','Upland',...
    'Location','eastoutside')
set(gca,'FontWeight','bold','FontSize',18)

for i = 1 : max(vegetation_class_complete)
    clear I
    I = find(vegetation_class_complete == i );
    
    plot3(centroids(I,1),centroids(I,2),centroids(I,3),'.','Color',colorVec(i,:),'Markersize',1); hold on
end

az = 0;
el = 90;
view(az, el);
axis square
axis equal
% title('MODEL DOMAIN: VEGETATION CLASS MAP w/ COMPLETE CLASSIFICATION')
title('out of bounds: 2 STD')
set(gca,'FontWeight','bold','FontSize',16)

% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])

% plot desired elements on top of map
plotpoints = outofbounds2std;
zzz = ones(length(plotpoints),1)*15;
plot3(centroids(plotpoints,1),centroids(plotpoints,2),zzz,'.k','Markersize',4);

% plot 3 std out of bounds
figure
hold on

% choose color scheme
% colorVec = hsv(max(vegetation_class_complete));
% colorVec = parula(max(vegetation_class_complete));
colorVec = jet(max(vegetation_class_complete));

a = NaN * ones(12);

for i = 1 : 12
    plot(a(i),'.','Color',colorVec(i,:),'markersize',50); % dummy plot (NaN's do not appear)
end
legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
    'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
    'S.virginica','Oyster bed','Open water','Upland',...
    'Location','eastoutside')
set(gca,'FontWeight','bold','FontSize',18)

for i = 1 : max(vegetation_class_complete)
    clear I
    I = find(vegetation_class_complete == i );
    
    plot3(centroids(I,1),centroids(I,2),centroids(I,3),'.','Color',colorVec(i,:),'Markersize',1); hold on
end

az = 0;
el = 90;
view(az, el);
axis square
axis equal
% title('MODEL DOMAIN: VEGETATION CLASS MAP w/ COMPLETE CLASSIFICATION')
title('out of bounds: 2 STD')
set(gca,'FontWeight','bold','FontSize',16)

% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])

% plot desired elements on top of map
plotpoints = outofbounds3std;
zzz = ones(length(plotpoints),1)*15;
plot3(centroids(plotpoints,1),centroids(plotpoints,2),zzz,'.k','Markersize',4);


%% Redefine out of bounds elements 
% Ater running 'Define vegetation class for mesh elements' some elements
% remain assigned because element centroidss did not fall within vegetation
% class polygons. Using mean elevation cover class data from Hladik's
% dissertation unassigned elements will be assigned tall, medium, or short
% S. alterniflora, of J. roemerianus based on element centroids elevation.


% Vegetation class elevation ranges (meters). Mid points between the mean
% elevations of a vegetation class

low_T_spartina = 0.293;
low_M_spartina = 0.768;
low_S_spartina = 1.023;
low_mud = 1.083;
low_S_virginica = 1.123;
low_B_maritima = 1.153;
low_salpan = 1.203;
low_J_roemerianus = 1.218;
low_B_frutescens = 1.328;
high_B_frutescens = 1.533;

% Mean vegetation class elevation and standard deviation
TSmean = 0.563;
MSmean = 0.973;
SSmean = 1.073;
MUDmean = 1.093;
SVmean = 1.153;
BMmean = 1.193;
SALTmean = 1.213;
JRmean = 1.223;
BFmean = 1.433;

TSSD = 0.27;
MSSD = 0.13;
SSSD = 0.1;
MUDSD = 0.1;
SVSD = 0.07;
BMSD = 0.07;
SALTSD = 0.06;
JRSD = 0.2;
BFSD = 0.1;

vegetation_class_complete_fixed = vegetation_class_complete;

% [ unassigned_elements ] = sub2ind ( size( vegetation_class ), find( vegetation_class == 0 ) );
unassigned_elements = outofbounds;
% unassigned_elements = outofboundsfixed;

% Define upland elements
% [ upland_elements ] = sub2ind ( size( centroids(:,3) ), find( centroids(unassigned_elements,3) > uplandelev) );
uplandelev = BFmean + BFSD;
[indUPLAND] = find( centroids(unassigned_elements,3) > uplandelev );

vegetation_class_complete_fixed( unassigned_elements( indUPLAND ) ) = 12;

% Open water elements
% All unassigned elements are labled as 0, which SHOULD be all elements
% with a centroids elevation less than low_T_spartina that is assigned
% above.
% [ openwater_elements ] = sub2ind ( size( vegetation_class_complete_fixed ), find( vegetation_class_complete_fixed == 0 ) );
% vegetation_class_complete_fixed( openwater_elements( : ) ) = 11;
waterelev = TSmean - TSSD;
[indWATER] = find( centroids(unassigned_elements,3) < waterelev );

vegetation_class_complete_fixed( unassigned_elements( indWATER ) ) = 11;

% [ unassigned_elements_after ] = find( vegetation_class_complete_fixed == 0 );

% Classify unassigned elements based on elevation ranges

% tall spartina
IndTS = find((centroids(unassigned_elements,3)>=low_T_spartina) & (centroids(unassigned_elements,3)<low_M_spartina));
vegetation_class_complete_fixed(unassigned_elements(IndTS)) = 8;

% medium spartina
IndMS = find((centroids(unassigned_elements,3)>=low_M_spartina) & (centroids(unassigned_elements,3)<low_S_spartina));
vegetation_class_complete_fixed(unassigned_elements(IndMS)) = 6;

% short spartina
IndSS = find((centroids(unassigned_elements,3)>=low_S_spartina) & (centroids(unassigned_elements,3)<low_mud));
vegetation_class_complete_fixed(unassigned_elements(IndSS)) = 7;

% mud
Indmud = find((centroids(unassigned_elements,3)>=low_mud) & (centroids(unassigned_elements,3)<low_S_virginica));
vegetation_class_complete_fixed(unassigned_elements(Indmud)) = 4;

% Sarcocornia
IndSV = find((centroids(unassigned_elements,3)>=low_S_virginica) & (centroids(unassigned_elements,3)<low_B_maritima));
vegetation_class_complete_fixed(unassigned_elements(IndSV)) = 9;

% Batis maritima
IndBM = find((centroids(unassigned_elements,3)>=low_B_maritima) & (centroids(unassigned_elements,3)<low_salpan));
vegetation_class_complete_fixed(unassigned_elements(IndBM)) = 2;

% salt pan
Indsalt = find((centroids(unassigned_elements,3)>=low_salpan) & (centroids(unassigned_elements,3)<low_J_roemerianus));
vegetation_class_complete_fixed(unassigned_elements(Indsalt)) = 5;

% J. roemerianus
IndJR = find((centroids(unassigned_elements,3)>=low_J_roemerianus) & (centroids(unassigned_elements,3)<low_B_frutescens));
vegetation_class_complete_fixed(unassigned_elements(IndJR)) = 3;

% B. frutescens
IndBF = find((centroids(unassigned_elements,3)>=low_B_frutescens) & (centroids(unassigned_elements,3)<=high_B_frutescens));
vegetation_class_complete_fixed(unassigned_elements(IndBF)) = 1;

% % open water
IndWater = find(centroids(unassigned_elements,3)<low_T_spartina);
vegetation_class_complete_fixed(unassigned_elements(IndWater)) = 11;

% % Upland
% Indupland = find(centroids(:,3)>high_B_frutescens);
% vegetation_class_complete(Indupland) = 12;

% Check that elements assigned using hyperspectral data have an elevation
% that falls one STD from the mean elevation for a given class
% Mean vegetation class elevation and standard deviation
TSmean = 0.563;
MSmean = 0.973;
SSmean = 1.073;
MUDmean = 1.093;
SVmean = 1.153;
BMmean = 1.193;
SALTmean = 1.213;
JRmean = 1.223;
BFmean = 1.433;

TSSD = 0.27;
MSSD = 0.13;
SSSD = 0.1;
MUDSD = 0.1;
SVSD = 0.07;
BMSD = 0.07;
SALTSD = 0.06;
JRSD = 0.2;
BFSD = 0.1;

% 1 STD
z = 1;
for i = 1 : length(vegetation_class_complete_fixed)
    %     i
    if vegetation_class_complete_fixed(i) == 1
        if (centroids(i,3)>=(BFmean-BFSD))
            if (centroids(i,3)<=(BFmean+BFSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    
    
    if vegetation_class_complete_fixed(i) == 2
        if (centroids(i,3)>=(BMmean-BMSD))
            if (centroids(i,3)<=(BMmean+BMSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    if vegetation_class_complete_fixed(i) == 3
        if (centroids(i,3)>=(JRmean-JRSD))
            if (centroids(i,3)<=(JRmean+JRSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    if vegetation_class_complete_fixed(i) == 4
        if (centroids(i,3)>=(MUDmean-MUDSD))
            if (centroids(i,3)<=(MUDmean+MUDSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    if vegetation_class_complete_fixed(i) == 5
        if (centroids(i,3)>=(SALTmean-SALTSD))
            if (centroids(i,3)<=(SALTmean+SALTSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
        
    end
    
    if vegetation_class_complete_fixed(i) == 6
        if (centroids(i,3)>=(MSmean-MSSD))
            if (centroids(i,3)<=(MSmean+MSSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
        
    end
    
    if vegetation_class_complete_fixed(i) == 7
        if (centroids(i,3)>=(SSmean-SSSD))
            if (centroids(i,3)<=(SSmean+SSSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    if vegetation_class_complete_fixed(i) == 8
        if (centroids(i,3)>=(TSmean-TSSD))
            if (centroids(i,3)<=(TSmean+TSSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
    if vegetation_class_complete_fixed(i) == 9
        if (centroids(i,3)>=(SVmean-SVSD))
            if (centroids(i,3)<=(SVmean+SVSD))
                %break
            else
                outofboundsfixed(z) = i;
                z = z + 1;
            end
        else
            outofboundsfixed(z) = i;
            z = z + 1;
        end
    end
    
end

% plot elements fixed
figure
hold on
colorVec = hsv(max(vegetation_class_complete_fixed));
% colorVec = parula(max(vegetation_class_complete_fixed));
a = NaN * ones(12);
% colormap hsv(12)
for i = 1 : 12
    plot(a(i),'.','Color',colorVec(i,:),'markersize',50); % dummy plot (NaN's do not appear)
end
% colormap jet
legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
    'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
    'S.virginica','Oyster bed','Open water','Upland',...
    'Location','eastoutside')
set(gca,'FontWeight','bold','FontSize',18)


colorVec = hsv(max(vegetation_class_complete));
% colorVec = jet(max(vegetation_class_complete));
% colorVec = parula(max(vegetation_class_complete_fixed));
for i = 1 : max(vegetation_class_complete_fixed)
    clear I
    I = find(vegetation_class_complete_fixed == i );
    %     zzz = ones(length(I),1)*15;
    %     plot3(centroids(I,1),centroids(I,2),zzz,'.','Color',colorVec(i,:),'Markersize',3); hold on
    
    plot3(centroids(I,1),centroids(I,2),centroids(I,3),'.','Color',colorVec(i,:),'Markersize',1); hold on
end
% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])

az = 0;
el = 90;
view(az, el);
axis square
axis equal
% title('MODEL DOMAIN: VEGETATION CLASS MAP ')
% title('VEGETATION CLASS MAP ')
title('Elements Fixed')
% legend('B.frutenscens','B.maritima','J.roemerianus','Intertidal mud',...
%     'Salt pan','S.alterniflora M','S.alterniflora S','S.alterniflora T',...
%     'S.virginica','Oyster bed','Open water','Upland',...
%     'Location','northwest')
set(gca,'FontWeight','bold','FontSize',18)
% xlim([4.7317e+05 4.7551e+05])
% ylim([3.4810e+06 3.4844e+06])
az = 0;
el = 90;
view(az, el);
axis equal

zzz = ones(length(outofboundsfixed),1)*15;
plot3(centroids(outofboundsfixed,1),centroids(outofboundsfixed,2),zzz,'.k','Markersize',3);

