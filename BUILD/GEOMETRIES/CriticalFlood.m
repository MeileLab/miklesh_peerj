%% CRITICAL FLOOD
% created by: David Miklesh
% date created: January 12, 2014
% date modified: January 28, 2015

%% CENTROID
tic

% complete mesh output file name
prefix = 'NDM20_unsmoothed';

% Input resolution in tidal range and above HHTide
% delta_htide = 0.01; % (meters)
% delta_hupland = 0.1; % (meters)
delta_htide = 0.5; % (meters)
delta_hupland = 0.5; % (meters)


load(strcat(prefix,'_mesh','.mat'))

% load a tidal signal to determine Highest High Tide (HHTide) and Lowest
% Low Tide (LLTide)
HHTide = max( nu )+0.2;
LLTide = min( nu );

% elements = test2d_elements;
% nodes = test2d_nodes;
% neighbors = test2d_neighbors;

start_time = datestr(now, 'd:HH:MM:SS');

%
% Pre-allocate critical_flooding matrix.
% Col.1 - node no.
%
lc = length( elements );
l = length( nodes );
crit_flood_elements = -99 * ones( lc, 1 );
%
% Create tide depth array.
%

h_low = min( centroids(:,3));
h_high = max( centroids(:,3));
h = LLTide : delta_htide : HHTide;
h = [h (HHTide+delta_hupland):delta_hupland:(h_high+delta_hupland)];


% Get initial flooding cell and nodes. By finding node with lowest
% elevation.
% [M,int_node]=min(nodes(:,3))
% [ int_cell, col] = ind2sub ( size(neighbors), find ( neighbors == int_node ) ); % find cells that share node.
% int_cell = int_cell(1); % more than one cell associated with node. Simply choose first one.
 
% int_node = 1; % chosen because nodal elevation is -2m. Assume anything below is flood.
% [ int_cell, col] = ind2sub ( size(elements), find ( elements == int_node ) );
% int_cell = int_cell(1); % more than one cell associated with node. Simply choose first one.

[ low_cells, ~] = ind2sub ( size(centroids), find ( centroids(:,3) < LLTide ) );

[ maxlowcell, ~] = ind2sub ( size(low_cells), find ( max(centroids(low_cells,3))));
int_cell = low_cells(maxlowcell); % Plot on mesh to determine is int_cell is a suitable location


% int_cell = 1; 
% Create checked array.
%'1' = inundated, '0' = not checked, '-1' = not inundated.
%
checked = zeros ( lc, 1 );

%
% Loop through tide heights to establish critical flooding depth
%
n = length ( h );
for i = 1 : n
    h_now = h(i)
    %
    % Create element queue
    %
    queue = int_cell;
    %
    % Create been_there array.
    %'1' = been_there, '0' = not been_there.
    %
    been_there = zeros ( lc, 1 );
    checked_new = zeros ( lc, 1 );
    %
    % Evaluate queue
    %
    while (~isempty(queue))
        cell = queue ( 1 );
        if checked_new ( cell) == 0
            
            
            %
            % Get element height 
            %
      
            height_here = centroids( cell, 3 );
            
            
            %
            % Compare centroidss heights to water level and checked array.
            % If true write crit_flood array.
            %
            t_here = 0;
            if ( checked_new ( cell ) ~= 1) % hasn't been assigned crit flood elevation
                if ( height_here <= h_now )
                    if ( crit_flood_elements ( cell ) == -99)
                        crit_flood_elements ( cell ) = h_now;
                    end
                    checked_new ( cell ) = 1; % mark that element has crit flood value
                    t_here = 1;
                    
                    %                     queue = [queue element_l element_m element_n];
                else
                    checked_new ( cell ) = -1; % not assigned crit flood value
                end
                %t_l = 1;
            end
            
            if ( t_here ==1 )
                
                if (( neighbors ( cell, 1 ) ~= -1 ) && ( checked_new ( neighbors ( cell, 1 ) ) == 0))
                    queue = [queue (neighbors ( cell , 1 ))];
                end
                
                if (( neighbors ( cell, 2 ) ~= -1 ) && ( checked_new ( neighbors ( cell, 2 ) ) == 0))
                    queue = [queue (neighbors ( cell, 2 ))];
                end
                
                if (( neighbors ( cell, 3 ) ~= -1) && ( checked_new ( neighbors ( cell, 3 ) ) == 0))
                    queue = [queue (neighbors ( cell, 3 ))];
                end
                
            end
            
           
            
            %
            % Pop value from front of queue
            %
            queue ( 1 ) = [];
        else
            queue ( 1 ) = [];
            
        end
    end
    %
    % Update checked with checked_new after each water level
    %
    
    index = checked == checked_new;
    I = find( index ~= 0 );
%     I = ind2sub ( size(index), find ( index ~= 0 ));
    checked ( I ) = checked_new ( I );
    
end

% elements with elevation lower than LLTide are assigned critflood
% elevation equal to centroids elevation
crit_flood_elements(low_cells,1) =  centroids(low_cells,3);

% save(filename,'crit_flood_elements','nodes','centroids','elements','neighbors','surface_area',...
%     'area_neighbor','area_neighbor_above','area_neighbor_below','alpha')

save(strcat(prefix,'_complete','.mat'),'nodes','centroids','elements','neighbors','surface_area',...
    'area_neighbor','area_neighbor_above','area_neighbor_below','alpha','DrainageDistance','VertDrainDist','crit_flood_elements','nu','S')

toc

start_time
end_time = datestr(now, 'd:HH:MM:SS')


%% PLOT INITIAL ELEMENT FOR CRITICALFLOOD
figure
Tri = elements ( :, 1:3);
trimesh ( Tri, nodes ( :, 1 ), nodes ( :, 2 ), nodes ( :, 3 ) ); hold on
az = 0;
el = 90;
view(az, el)
colormap jet
colorbar

axis square

% plot3(centroids(int_cell,1),centroids(int_cell,2),15,'*k','Markersize',3); hold on
plot3(centroids(int_cell,1),centroids(int_cell,2),centroids(int_cell,3),'*k','Markersize',8); hold on

