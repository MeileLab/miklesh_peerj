%% Salt Model Initial Computations

%------------------------------------------------------------------------------
% Specify mesh input file type
%------------------------------------------------------------------------------
InputType = 2; % Mobj = 1, SMS = 2, .txt = 3, or .mat = 4
prefix = 'NDM20_unsmoothed'; 

% .txt files
delimiterIn = '\t'; % '\t' for tab, ',' for comma, and ' ' for space
headerlinesIn = 0; % numer of text header lines in ASCII file: nonnegative scalar integer


if InputType == 1
    
    nodes(:,1) = Mobj.x;
    nodes(:,2) = Mobj.y;
    nodes(:,3) = Mobj.h * -1;
    elements = Mobj.tri;
    
elseif InputType == 2
    
    Mobj = read_sms_mesh_dd('2dm',strcat(prefix,'.2dm'),'bath',strcat(prefix,'.pts'));
    nodes(:,1) = Mobj.x;
    nodes(:,2) = Mobj.y;
    nodes(:,3) = Mobj.h * -1;
    elements = Mobj.tri;
    
elseif InputType ==3
    
    nodes = importdata(strcat(prefix,'_nodes.txt'));
    elements = importdata(strcat(prefix,'_elements.txt'));
    
elseif InputType ==4
    
    load(strcat(prefix,'.mat'))
end

%------------------------------------------------------------------------------
% Plot mesh
%------------------------------------------------------------------------------
figure
Tri = elements(:,1:3);
trimesh(Tri,nodes(:,1),nodes(:,2),nodes(:,3)); hold on
colormap jet
c = colorbar;
c.Label.String = 'Elevation (m)';
axis equal
xlabel('Easting (UTM)')
ylabel('Northing (UTM)')
h = gca;
h.XTickLabelRotation=45;
az = 0;
el = 90;
view(az, el);
axis equal

%------------------------------------------------------------------------------
% Compute centroids
%------------------------------------------------------------------------------
disp('Computing centroids')

tic
% x_cent
centroids(:,1) = (nodes(elements(:,1),1) + nodes(elements(:,2),1) ...
    + nodes(elements(:,3),1)) / 3;
% y_cent
centroids(:,2) = (nodes(elements(:,1),2) + nodes(elements(:,2),2) ...
    + nodes(elements(:,3),2)) / 3;
% z_cent
centroids(:,3) = (nodes(elements(:,1),3) + nodes(elements(:,2),3) ...
    + nodes(elements(:,3),3)) / 3;
toc

%------------------------------------------------------------------------------
% Compute neighbors
%------------------------------------------------------------------------------
disp('Computing neighbors');
tic
% create .txt files needed for neighbors function
dlmwrite(strcat(prefix,'_elements.txt'), elements, 'delimiter', '\t')
dlmwrite(strcat(prefix,'_nodes.txt'), nodes, 'delimiter', '\t')

triangulation_triangle_neighbors( prefix )

neighbors = importdata(strcat(prefix,'_element_neighbors.txt'));

toc

%------------------------------------------------------------------------------
% Compute surface area
%------------------------------------------------------------------------------
side_a = sqrt((nodes(elements(:,3),1) - nodes(elements(:,2),1)).^2 ...
    + (nodes(elements(:,3),2) -  nodes(elements(:,2),2)).^2);

% length side_b ( meters )
side_b = sqrt((nodes(elements(:,1),1) - nodes(elements(:,3),1)).^2 ...
    + (nodes(elements(:,1),2) -  nodes(elements(:,3),2)).^2);

% length side_c ( meters )
side_c = sqrt((nodes(elements(:,2),1) - nodes(elements(:,1),1)).^2 ...
    + (nodes(elements(:,2),2) -  nodes(elements(:,1),2)).^2 );

% calculate semiperimeter
s = (side_a + side_b + side_c) / 2;

% area of triangle with side lengths side_a, side_b, side_c. (squared meters)
surface_area = sqrt(s .* (s - side_a) .* (s - side_b) .* (s - side_c));
toc

%------------------------------------------------------------------------------
% Compute horizontal distance btween neightbor centroids and horizontal flux area
%------------------------------------------------------------------------------
disp('Compute horizontal distance between centroids');
tic
% global depth;
depth = 0.3;

area_neighbor = zeros(length(neighbors),3);
area_neighbor_above = zeros(length(neighbors),3);
area_neighbor_below = zeros(length(neighbors),3);
node_distance = zeros(length(neighbors),3);

parfor xx = 1 : length(neighbors)
    for yy = 1 : 3
        neighbor = neighbors(xx,yy);
        if neighbor == -1
            dx(xx,yy) = -1;
        else
            dx(xx,yy) = sqrt((centroids(xx,1) - centroids(neighbor,1))^2 ...
                + (centroids(xx,2) - centroids(neighbor,2))^2);
        end
        
        % Compute vertical area between neighbor elements (squared meters)
        if neighbors(xx,yy) == -1
            area_neighbor(xx,yy) = 0;
            area_neighbor_above(xx,yy) = 0;
            area_neighbor_below(xx,yy) = 0;
        else
            neighbor = neighbors(xx,yy);
            
            a_neighbor = elements(xx,:);
            b_neighbor = elements(neighbor,:);
            
            shared_nodes = intersect(a_neighbor,b_neighbor);
            
            area_neighbor(xx,yy) = sqrt((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1)).^2 ...
                + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2) * depth;
            
            node_distance(xx,yy) = sqrt((nodes(shared_nodes(1), 1 ) - nodes(shared_nodes(2 ), 1 ) ).^2 ...
                + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2);
            
            % pedon disconnected from neighboring pedon
            if (centroids(xx,3) - depth) >= centroids(neighbor,3) % higher element
                area_neighbor_above(xx,yy) = sqrt ( (nodes(shared_nodes(1 ), 1 ) - nodes(shared_nodes(2 ), 1 ) ).^2 ...
                    + (nodes(shared_nodes(1 ), 2 ) - nodes(shared_nodes(2 ), 2 ) ).^2 ) * (centroids(xx,3) - centroids(neighbor,3));
                % drainage face
                area_neighbor_below(xx,yy) = sqrt ((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1)).^2 ...
                    + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2) * depth;
                
            elseif centroids(xx,3) <= (centroids(neighbor,3) - depth) % lower element
                area_neighbor_above (xx,yy) = 0;
                % drainage face
                area_neighbor_below(xx,yy) = sqrt ((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1)).^2 ...
                    + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2) * depth;
                
                % pedon share flux area with neighboring pedon
            elseif centroids(xx,3) >= centroids(neighbor,3)
                if (centroids(xx,3) - depth) < centroids(neighbor,3) % higher element
                    area_neighbor_above(xx,yy) = sqrt((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1)).^2 ...
                        + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2) * (centroids(xx,3) - centroids(neighbor,3));
                    area_neighbor_below(xx,yy) = sqrt((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1)).^2 ...
                        + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2)).^2) * depth;
                elseif centroids(xx,3) == centroids(neighbor,3) % elements same
                    area_neighbor_above(xx,yy) = 0;
                    area_neighbor_below(xx,yy) = sqrt((nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1) ).^2 ...
                        + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2) ).^2 ) * depth;
                end
            elseif centroids(xx,3) < centroids(neighbor,3) % lower element
                if centroids(xx,3) > (centroids(neighbor,3) - depth)
                    area_neighbor_above(xx,yy) = 0;
                    area_neighbor_below(xx,yy) = sqrt ( (nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1) ).^2 ...
                        + (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2) ).^2 ) * (centroids(xx,3) - (centroids(neighbor,3) - depth));
                end
                
            end
        end
    end
end
toc
%%
%------------------------------------------------------------------------------
% Compute normal vector angle between two nodes
%------------------------------------------------------------------------------
disp('normal vector between 2 nodes')
tic
parfor xx = 1 : length ( neighbors )
    for yy = 1 : 3
        neighbor = neighbors (xx,yy);

        if neighbors (xx,yy) == -1
            normal_neighbor (xx,yy) = -1;
        else
            a_neighbor = elements ( xx, : );
            b_neighbor = elements ( neighbor, : );
            
            shared_nodes = intersect ( a_neighbor, b_neighbor);
            
            normal_neighbor (xx,yy) =   atand ( - ( (nodes(shared_nodes(1),2) - nodes(shared_nodes(2),2) ) ...
                / (nodes(shared_nodes(1),1) - nodes(shared_nodes(2),1) ) )^-1 );
        end
    end
end
toc

%------------------------------------------------------------------------------
% Compute vector angle between two centroids
%------------------------------------------------------------------------------
disp('angle between 2 centroids')
tic
parfor xx = 1 : length(neighbors)
    for yy = 1 : 3
        neighbor = neighbors(xx,yy);
        if neighbor == -1
            angle_centroids(xx,yy) = -1;
        else
            angle_centroids(xx,yy) = atand ((centroids( xx,2) - centroids ( neighbor,2)) ...
                / (centroids(xx,1) - centroids(neighbor,1)));
        end
    end
end
toc

%------------------------------------------------------------------------------
% Compute angle alpha_angle. Subtract angle between centroids from the normal side vector.
%------------------------------------------------------------------------------
disp('compute angle alpha_angle')
tic
for xx = 1 : length(neighbors)
    for yy = 1 : 3
        neighbor = neighbors(xx,yy);
        if neighbor == -1
            alpha_angle(xx,yy) = NaN;
        else
            alpha_angle(xx,yy) =  (normal_neighbor(xx,yy) - angle_centroids(xx,yy));
            
            if (alpha_angle(xx,yy) > 90)
                alpha_angle(xx,yy) = 180 - alpha_angle(xx,yy);
            end
            if (alpha_angle(xx,yy) < 0)
                alpha_angle(xx,yy) = - alpha_angle(xx,yy);
            end
        end
    end
end
toc

alpha = alpha_angle; 

%------------------------------------------------------------------------------
% Identify centroids below specified elevation
%------------------------------------------------------------------------------

DupRiverElevation = -3; % Duplin channel
DuplinRivercentroids = find(centroids(:,3) < DupRiverElevation);

DupCreekElevation = -0.4; % side creeks
DuplinCreekcentroids = find(((centroids(:,3) >= DupRiverElevation) & (centroids(:,3) < DupCreekElevation)));
RiverCreekcentroids = vertcat(DuplinCreekcentroids, DuplinRivercentroids);
NonRiverCreekcentroids = find(centroids(:,3) >= DupCreekElevation);

% Checks RiverCreek and NonRiverCreek total the number of elements in the
% mesh
ADD = length(RiverCreekcentroids) + length(NonRiverCreekcentroids);


%------------------------------------------------------------------------------
% Find dx for each NonRiverCreekcentroids
%------------------------------------------------------------------------------

% Find distance from every 'NonRiverCreekcentroids' to
% every 'RiverCreekcentroids' then find the minumum distance and index
DrainageDistance = NaN * ones(length(NonRiverCreekcentroids),2);
tempdistance = NaN * ones(length(NonRiverCreekcentroids),1);


for i = 1 : length(NonRiverCreekcentroids)
    tempdistance = sqrt((centroids(NonRiverCreekcentroids(i),1)-centroids(RiverCreekcentroids(:),1)).^2 ...
        + (centroids(NonRiverCreekcentroids(i),2)-centroids(RiverCreekcentroids(:),2)).^2);
    [minDistance,indMin] = min(tempdistance);
    DrainageDistance(i,1) = minDistance; % nearest drainage element from NonRiverCreekcentroids (meters)
    DrainageDistance(i,2) = RiverCreekcentroids(indMin); % nearest mesh element
end

VertDrainDist = NaN * ones(length(centroids),2);
VertDrainDist(NonRiverCreekcentroids,1) = DrainageDistance(:,2);
VertDrainDist(NonRiverCreekcentroids,2) = DrainageDistance(:,1);

%------------------------------------------------------------------------------
% Plot the elements the NonRiverCreekcentroids draining into
%------------------------------------------------------------------------------

tri = elements;
x = nodes(:,1);
y = nodes(:,2);
z = nodes(:,3);
tr = triangulation(tri,x(:),y(:),z(:));

% Plot traingular mesh
figure('Name','Triangular Mesh Plot','NumberTitle','off','position', [500, 500, 1000, 750])
h = trimesh(tr); hold on
az = 0;
el = 90;
view(az, el);
colormap jet
colorbar
axis equal
title('Triangular Mesh Plot')
xlabel('Easting (UTM)')
ylabel('Northing (UTM)')
h = gca;
h.XTickLabelRotation=45;
 
markersize = 10;
hold on
plot3(centroids(DrainageDistance(:,2),1),centroids(DrainageDistance(:,2),2),centroids(DrainageDistance(:,2),3) + 0.1,'.k','MarkerSize',markersize)


% save incase of error
save(strcat(prefix,'_mesh','.mat'),'nodes','centroids','elements','neighbors','surface_area',...
    'area_neighbor','area_neighbor_above','area_neighbor_below','alpha','VertDrainDist')








