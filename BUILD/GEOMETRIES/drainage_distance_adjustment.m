%% drainage distance adjustment
% Run after hyperspectral classification to correct drainage reclassified
% elements

neighbors = neighbors_variable;
for i = 1:3
    for j = 1 : length(neighbors)
        if neighbors(j,i) == -1
            neighbors(j,i) = NaN; 
        end
    end
end

for i = 1  : length(VertDrainDist(:,2))
    veg = vegetation_class_complete(i);
    if veg == 1 || veg == 2|| veg == 3|| veg == 6|| veg == 7|| veg == 8|| veg == 9|| veg == 12
        if isnan(VertDrainDist(i,2))
            X = neighbors(i,:);
            X = X(~isnan(X));
            VertDrainDist(i,2) = nanmean(VertDrainDist(X,2),1);
            if isnan(VertDrainDist(i,2))
                ind = find(vegetation_class_complete == vegetation_class_complete(i));
                 VertDrainDist(i,2) = nanmean(VertDrainDist(ind,2),1);
            end
        end
    else
        VertDrainDist(i,1:2) = nan;
    end
    
end


%% check

 ind = find(vegetation_class_complete == 10);
a = isnan(VertDrainDist(ind,1));
sum(a)
